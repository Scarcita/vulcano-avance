import React, { useEffect, useState } from "react";
import Image from "next/image";
import Layout from "../../components/layout";
import Spinner from "react-activity/dist/Spinner";
import "react-activity/dist/Spinner.css";

import ImgRepuestos from "../../public/Desktop12/repuestos.svg";
import ImgCosto from "../../public/Desktop12/costo.svg";

import ListOTsCerradas from "../../components/Invoicing/Dashboard/listOTsCerradas";
import BarChartWork from "../../components/Invoicing/Dashboard/BarChartWork";
import BarCharFacturacion from "../../components/Invoicing/Dashboard/BarChartFacturacion";
import BarCharService from "../../components/Invoicing/Dashboard/BarChartService";
import CounterTotalVentas from "../../components/Invoicing/Dashboard/counterTotalVentas";
import CounterventasMesAnterior from "../../components/Invoicing/Dashboard/counterVentasMesAnt";
import OrdersCreatedThisMonth from "../../components/Invoicing/Dashboard/ordersCreatedThisMonth";
import OrdersCreatedLastMonth from "../../components/Invoicing/Dashboard/ordersCreatedLastMonth";
import ServicesThisMonthCount from "../../components/Invoicing/Dashboard/servicesThisMonthCount";
import BarChartExternals from "../../components/Invoicing/Dashboard/BarChartExternals";
import PartsThisMonth from "../../components/Invoicing/Dashboard/partsThisMonth";
import ExternalsThisMonth from "../../components/Invoicing/Dashboard/externalsThisMonth";
import PartsThisMonthCount from "../../components/Invoicing/Dashboard/partsThisMonthCount";
import ServicesThisMonth1 from "../../components/Invoicing/Dashboard/servicesThisMonth";
import ExternalsThisMonthCount from "../../components/Invoicing/Dashboard/externalsThisMonthCount";
// import GaugeChartControl from "../../components/Invoicing/Dashboard/gaugeChart";
// import GaugeChartCreatedOrdersMonth from "../../components/Invoicing/Dashboard/gaugeChartThisMonthVsLastMonth";

export default function Facturacion() {
  const [showDots, setShowDots] = useState(true);

  const [data, setData] = useState([]);

  useEffect(() => {
    fetch("https://slogan.com.bo/vulcano/orders/total/abierto")
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          console.log(data.data);
          setData(data.data);
        } else {
          console.error(data.error);
        }
      })
      .then(setShowDots(false));
  }, []);

  return showDots ? (
    <div className="flex justify-center items-center mt-[20px]">
      <Spinner
        color="#3682F7"
        size={17}
        speed={1}
        animating={true}
        style={{ marginLeft: "auto", marginRight: "auto" }}
      />
    </div>
  ) : (
    <div className="flex h-full flex-col bg-[#F6F6FA]">
      <div className=" mt-[26px] ml-[20px] mr-[20px]">
        <div>
          <div className="flex flex-row">
            <h2 className="text-[24px] font-semibold">Facturacion</h2>
            <h2 className="text-[14px] pt-[10px]">/Mes actual</h2>
          </div>
        </div>
        <div className="mt-[20px]">
          <div className="mt-[20px] grid grid-cols-12 gap-4">
            <div className="col-span-12 md:col-span-6 lg:col-span-4 flex flex-row bg-[#FFFF] rounded-[16px] shadow-md  justify-between py-2 px-2">
              <div className="grid grid-cols-12 gap-4">
                  <div className="col-span-6 md:col-span-6 lg:col-span-6">
                    <div>
                      <div className="text-[14px] text-[#000000] ">
                        # servicios Requeridos
                      </div>
                      <div className="w-full flex flex-row justify-between">
                      <div className="text-[34px] text-[#3682F7] items-center self-center justify-center">
                        <ServicesThisMonth1/>
                      </div>
                      <div className=" flex flex-row h-[20px] bg-[#3682F7] rounded-[5px] items-center self-center justify-center p-1">
                        <div className="w-[16px] h-[9px] bg-[#FFFFFF] rounded-[2.5px] items-center self-center justify-center ">
                          <p className="text-[#3682F7] text-[12px] font-semibold mt-[-6px] ml-[3px]">
                            ↓
                          </p>
                        </div>
                        <text className="text-[12px] text-[#FFFFFF] font-semibold pl-[3px]">
                          15%
                        </text>
                      </div>
                    </div>
                    <div className="text-[10px] text-[#A5A5A5]">
                      Este Mes
                    </div>
                  </div>
                </div>
                <div className="col-span-6 md:col-span-6 lg:col-span-6 text-center self-center">
                  <BarCharFacturacion />
                </div>
              </div>
            </div>
            <div className="col-span-12 md:col-span-6 lg:col-span-4 flex flex-row bg-[#FFFF] rounded-[16px] shadow-md  justify-between py-2 px-2">
              <div className="grid grid-cols-12 gap-4">
                <div className="col-span-6 md:col-span-6 lg:col-span-6">
                  <div>
                    <div className="text-[14px] text-[#000000] ">
                      # Partes
                    </div>
                    <div className="w-full flex flex-row justify-between">
                    <div className="text-[34px] text-[#3682F7] items-center self-center justify-center">
                      <PartsThisMonth/>
                    </div>
                    <div className=" flex flex-row h-[20px] bg-[#3682F7] rounded-[5px] items-center self-center justify-center p-1">
                      <div className="w-[16px] h-[9px] bg-[#FFFFFF] rounded-[2.5px] items-center self-center justify-center ">
                        <p className="text-[#3682F7] text-[12px] font-semibold mt-[-6px] ml-[3px]">
                          ↓
                        </p>
                      </div>
                      <text className="text-[12px] text-[#FFFFFF] font-semibold pl-[3px]">
                        15%
                      </text>
                    </div>
                  </div>
                  <div className="text-[10px] text-[#A5A5A5]">
                    Este Mes
                  </div>
                </div>
              </div>
              <div className="col-span-6 md:col-span-6 lg:col-span-6 text-center self-center">
                <BarCharService />
              </div>
            </div>
            </div>
            <div className="col-span-12 md:col-span-6 lg:col-span-4 flex flex-row bg-[#FFFF] rounded-[16px] shadow-md  justify-between py-2 px-2">
              <div className="grid grid-cols-12 gap-4">
                <div className="col-span-6 md:col-span-6 lg:col-span-6">
                    <div>
                      <div className="text-[14px] text-[#000000] ">
                        # Trabajos Externos
                      </div>
                      <div className="w-full flex flex-row justify-between">
                      <div className="text-[34px] text-[#3682F7] items-center self-center justify-center">
                        <ExternalsThisMonth/>
                      </div>
                      <div className=" flex flex-row h-[20px] bg-[#3682F7] rounded-[5px] items-center self-center justify-center p-1">
                        <div className="w-[16px] h-[9px] bg-[#FFFFFF] rounded-[2.5px] items-center self-center justify-center ">
                          <p className="text-[#3682F7] text-[12px] font-semibold mt-[-6px] ml-[3px]">
                            ↓
                          </p>
                        </div>
                        <text className="text-[12px] text-[#FFFFFF] font-semibold pl-[3px]">
                          15%
                        </text>
                      </div>
                    </div>
                    <div className="text-[10px] text-[#A5A5A5]">
                      Este Mes
                    </div>
                  </div>
                </div>
              <div className="col-span-6 md:col-span-6 lg:col-span-6 text-center self-center">
                <BarChartExternals/>
              </div>
              </div>
            </div>
            <div className="col-span-12 md:col-span-6 lg:col-span-4 flex flex-row bg-[#FFFF] rounded-[16px] shadow-md  justify-between py-2 px-2">
              <div>
                <div className="text-[14px] text-[#000000] ">
                  Ventas-Servicios Requeridos
                </div>
                <div className="text-[40px] text-[#3682F7]">
                  <ServicesThisMonthCount/>
                </div>
                <div className="text-[10px] text-[#A5A5A5]">Este Mes</div>
              </div>
              <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full items-center self-center pt-[10px] text-center">
                <Image src={ImgRepuestos} layout="fixed" alt="ImgRepuestos" />
              </div>
            </div>

            <div className="col-span-12 md:col-span-6 lg:col-span-4 flex flex-row bg-[#FFFF] rounded-[16px] shadow-md justify-between py-2 px-2">
              <div>
                <div className="text-[14px] text-[#000000] ">Venta Partes</div>
                <div className="text-[40px] text-[#3682F7]">
                  <PartsThisMonthCount/>
                </div>
                <div className="text-[10px] text-[#A5A5A5]">Este Mes</div>
              </div>
              <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full items-center self-center pt-[10px] text-center">
                <Image src={ImgRepuestos} layout="fixed" alt="ImgRepuestos" />
              </div>
            </div>

            <div className="col-span-12 md:col-span-6 lg:col-span-4 flex flex-row bg-[#FFFF] rounded-[16px] shadow-md justify-between py-2 px-2">
              <div>
                <div className="text-[14px] text-[#000000] ">Ventas - Trabajos externos</div>
                <div className="text-[40px] text-[#3682F7]">
                <ExternalsThisMonthCount/>
                </div>
                <div className="text-[10px] text-[#A5A5A5]">Este Mes</div>
              </div>
              <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full items-center self-center pt-[10px] text-center">
                <Image src={ImgRepuestos} layout="fixed" alt="ImgRepuestos" />
              </div>
            </div>
          </div>
        </div>
        <div className="mt-[26px] grid grid-cols-12 gap-4">
          <div className="col-span-12 md:col-span-12 lg:col-span-8">
            <div className="flex flex-row">
              <h2 className="text-[24px] font-semibold">Facturacion</h2>
              <h2 className="text-[14px] pt-[10px]">/Mes Actual vs Anterior</h2>
            </div>
            <div className="mt-[20px] grid grid-cols-12 gap-4 ">
              <div className="col-span-12 md:col-span-6 lg:col-span-6">
                <div className="flex flex-row bg-[#FFFF] rounded-[16px] shadow-md items-center self-center justify-between py-2 px-2">
                  <div>
                    <div className="text-[14px] text-[#000000] ">
                      Total ventas
                    </div>
                    <div className="text-[40px] text-[#3682F7]">
                      <CounterTotalVentas />
                    </div>
                    <div className="text-[10px] text-[#A5A5A5]">Este mes</div>
                  </div>
                  <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full items-center self-center text-center pt-[10px] ">
                    <Image src={ImgCosto} layout="fixed" alt="ImgCosto" />
                  </div>
                </div>
                <div className="flex flex-row mt-[17px]  bg-[#FFFF] rounded-[16px] shadow-md  justify-between py-2 px-2">
                  <div>
                    <div className="text-[14px] text-[#000000] ">
                      Total ventas
                    </div>
                    <div className="text-[40px] text-[#3682F7]">
                      <CounterventasMesAnterior/>
                    </div>
                    <div className="text-[10px] text-[#A5A5A5]">
                      Mes anterior
                    </div>
                  </div>
                  <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full items-center self-center text-center pt-[10px] ">
                    <Image src={ImgCosto} layout="fixed" alt="ImgCosto" />
                  </div>
                </div>
              </div>

              <div className="col-span-12 md:col-span-6 lg:col-span-6 bg-[#FFFF] flex flex-col rounded-[24px] justify-center items-center text-[#000000] ">
                {/* <GaugeChartControl/> */}
                <p>Mes actual vs anterior</p>
              </div>
            </div>
          </div>
          <div className="col-span-12 md:col-span-12 lg:col-span-4">
            <div className="flex flex-row">
              <h2 className="text-[24px] font-semibold">OTs cerradas</h2>
              <h2 className="text-[14px] pt-[10px]">
                /Control de calidad realizado
              </h2>
            </div>
            <div className="mt-[20px]">
              <ListOTsCerradas />
            </div>
          </div>
        </div>

        <div className=" mt-[26px] mt-[20px] ">
          <div className="flex flex-row">
            <h2 className="text-[24px] font-semibold">Ordenes de trabajo</h2>
            <h2 className="text-[14px] pt-[10px]">/Mes Actual vs Anterior</h2>
          </div>

          <div className="mt-[20px] grid grid-cols-12 gap-4  ">
            <div className="col-span-12 md:col-span-6 lg:col-span-3">
              <div className="flex flex-row  bg-[#FFFF] rounded-[16px] justify-between shadow-md py-2 px-2">
                <div>
                  <div className="text-[40px] text-[#3682F7] font-semibold">
                    <OrdersCreatedThisMonth/>
                  </div>
                  <div className="text-[10px] text-[#A5A5A5]">Mes actual</div>
                </div>
                <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full items-center self-center pt-[10px] text-center">
                  <Image src={ImgCosto} layout="fixed" alt="ImgCosto" />
                </div>
              </div>
              <div className="flex flex-row mt-[17px] bg-[#FFFF] rounded-[16px] justify-between  shadow-md py-2 px-2">
                <div>
                  <div className="text-[40px] text-[#3682F7] font-semibold">
                    <OrdersCreatedLastMonth/>
                  </div>
                  <p className="text-[10px] text-[#A5A5A5]">Mes anterior</p>
                </div>
                <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full items-center self-center pt-[10px] text-center">
                  <Image src={ImgCosto} layout="fixed" alt="ImgCosto" />
                </div>
              </div>
            </div>
            <div className="col-span-12 md:col-span-6 lg:col-span-3 bg-[#FFFF] flex flex-col rounded-[24px] justify-center items-center text-[#000000]  ">
              {/* <GaugeChartCreatedOrdersMonth/> */}
              <p className="mt-5 ">Mes actual vs anterior</p>
            </div>
            <div className="col-span-12 md:col-span-6 lg:col-span-2">
              <div className="text-center">
                <div className=" flex flex-row h-[18px] bg-[#3682F7] rounded-[5px] justify-center ml-[150px] mr-[150px] md:ml-[120px] md:mr-[120px] lg:ml-[60px] lg:mr-[60px]">
                  <div className="w-[16px] h-[9px] bg-[#FFFFFF] rounded-[2.5px] mt-[5px] ml-[3px] ">
                    <p className="text-[#3682F7] text-[12px] font-semibold mt-[-6px]">
                      ↓
                    </p>
                  </div>
                  <text className="text-[10px] text-[#FFFFFF] font-semibold ml-[3px] mt-[1px]">
                    15%
                  </text>
                </div>
                <h2 className="text-[32px]">{data}</h2>
                <h2 className="text-[11px]">OTs creadas</h2>
              </div>

              <div className="text-center">
                <div className="mt-[20px] flex flex-row h-[18px] bg-[#EE002B] rounded-[5px] text-center justify-center ml-[150px] mr-[150px] md:ml-[120px] md:mr-[120px] lg:ml-[60px] lg:mr-[60px]">
                  <div className="w-[16px] h-[9px] bg-[#FFFFFF] rounded-[2.5px] mt-[5px] ml-[3px]">
                    <p className="text-[#EE002B] text-[12px] font-semibold mt-[-6px]">
                      ↓
                    </p>
                  </div>
                  <text className="text-[10px] text-[#FFFFFF] font-semibold ml-[3px] mt-[1px]">
                    15%
                  </text>
                </div>
                <h2 className="text-[32px]">70</h2>
                <h2 className="text-[11px]">OTs desde citas</h2>
              </div>
            </div>

            <div className="col-span-12 md:col-span-6 lg:col-span-4 ">
              <BarChartWork />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
Facturacion.auth = true;
Facturacion.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};
