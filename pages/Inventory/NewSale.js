import React, { useEffect, useState } from "react";
import Spinner from "react-activity/dist/Spinner";
import "react-activity/dist/Spinner.css";

import "react-activity/dist/Spinner.css";

import Table from "../../components/Inventory/NewSale/Table";
import ColumnaDerecha from "../../components/Inventory/NewSale/ColumnaDerecha";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import Layout from "../../components/layout";
function PantallaTrece() {
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState([]);

  const getDatos = () => {
    setIsLoading(true);
    fetch("https://slogan.com.bo/vulcano/products/all")
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          console.log(data.data);
          setData(data.data);
        } else {
          console.error(data.error);
        }
        setIsLoading(false);
      });
  };
  useEffect(() => {
    getDatos();
  }, []);

  const [dataColumnaDerecha, setDataColumnaDerecha] = useState([]);

  const removeFromCart = (indextoBeremoved) => {
    setDataColumnaDerecha((products) =>
      products.filter((_, index) => index !== indextoBeremoved)
    );
    //setDataColumnaDerecha();
  };

  const ValidationSchema = yup.object().shape({
    nombre: yup.string().required("Es requerido"),
    nombreRazon: yup.string().required("Es requerido"),
    productos: yup.string().required("Es requerido"),
    numero: yup.string().required("Es requerido"),
  });
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(ValidationSchema),
  });

  const onSubmit = (data) => {
    console.log(data);
  };

  return (
    <div className="mt-[26px] ml-[20px] mr-[20px]">
      <div className="flex flex-row">
        <h2 className="text-[24px] font-semibold">Ventas por mostrador</h2>
        <h2 className="text-[24px]">/Nueva</h2>
      </div>
      <div>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="grid grid-cols-12 gap-6">
            <div className="col-span-12 md:col-span-12 lg:col-span-8">
              <div className="mt-[20px]">
                <p className="text-[18px] font-semibold">Cliente</p>
              </div>
              <div className="grid grid-cols-12 mt-[15px]">
                <div className="col-span-12 md:col-span-12 lg:col-span-12">
                  <div className="flex">
                    <input
                      // value={search}
                      // onChange={searcher}
                      type="text"
                      placeholder="77777"
                      className="bg-[#FFFF] w-full h-[30px] rounded-[8px] border-[#d2d2d2] border-[1px] pl-[10px] pr-[10px] mr-[10px]"
                    />
                    <button className="w-[30px] h-[30px] rounded-lg border-[#3682F7] border-[1px] text-center hover:bg-[#3682F7] pl-[3px] self-center">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth={1.5}
                        stroke="currentColor"
                        className="w-5 h-5 text-[#3682F7] hover:text-[#fff] self-center text-center"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
                        />
                      </svg>
                    </button>
                  </div>
                </div>
              </div>
              <div className="mt-[20px]">
                <p className="text-[18px] font-semibold">
                  Detalles de Facturación
                </p>
              </div>

              <div className="grid grid-cols-12 gap-3 mt-[15px] bg-[#fff] rounded-lg py-3 px-3">
                <div className="col-span-12 md:col-span-12 lg:col-span-6 ">
                  <label className="text-[14px] text-normal">CI/NIT</label>
                  <input
                    className={`form-control ${
                      errors.numero ? "is-invalid" : ""
                    } w-full h-8 border-[#D3D3D3] border rounded-lg pl-2`}
                    {...register("numero")}
                    placeholder="8781377017"
                  />
                </div>
                <div className="col-span-12 md:col-span-12 lg:col-span-6 self-center">
                  <label className="text-[14px] text-normal">
                    Nombre/Razón Social
                  </label>
                  <input
                    className={`form-control ${
                      errors.nombreRazon ? "is-invalid" : ""
                    } w-full h-8 border-[#D3D3D3] border rounded-lg pl-2`}
                    {...register("nombreRazon")}
                    placeholder="WANT Digital Agency"
                  />
                </div>
              </div>

              <div className="mt-[20px]">
                <p className="text-[18px] font-semibold">Productos</p>
              </div>

              <div className="grid grid-cols-12 mt-[22px]">
                <div className="col-span-12 md:col-span-12 lg:col-span-12">
                  <Table
                    isLoading={isLoading}
                    setIsLoading={setIsLoading}
                    getDatos={getDatos}
                    data={data}
                    setDataColumnaDerecha={setDataColumnaDerecha}
                  />
                </div>
              </div>
            </div>

            <div className="col-span-12 md:col-span-12 lg:col-span-4">
              <div className="mt-[10px]">
                <p className="text-[18px] font-semibold">Pedido</p>
              </div>
              <div className="mt-[20px]">
                <ColumnaDerecha
                  getDatos={getDatos}
                  data={data}
                  dataColumnaDerecha={dataColumnaDerecha}
                  setDataColumnaDerecha={setDataColumnaDerecha}
                  removeFromCart={removeFromCart}
                />
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}
PantallaTrece.auth = true;
PantallaTrece.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};
export default PantallaTrece;
