import { useEffect, useRef, useState } from "react";
import Layout from "../../components/layout";

import imagenFord from '../../assets/ford.png';

export default function AddMedia() {
  const canvasRef = useRef(null);

  const [circles, setCircles] = useState([]);
  const [crosses, setCrosses] = useState([]);

  const [shapeType, setShapeType] = useState("circle");
  
  const [shapesAdded, setShapesAdded] = useState([]);


  const HOOK_SVG =
  'm129.03125 63.3125c0-34.914062-28.941406-63.3125-64.519531-63.3125-35.574219 0-64.511719 28.398438-64.511719 63.3125 0 29.488281 20.671875 54.246094 48.511719 61.261719v162.898437c0 53.222656 44.222656 96.527344 98.585937 96.527344h10.316406c54.363282 0 98.585938-43.304688 98.585938-96.527344v-95.640625c0-7.070312-4.640625-13.304687-11.414062-15.328125-6.769532-2.015625-14.082032.625-17.960938 6.535156l-42.328125 64.425782c-4.847656 7.390625-2.800781 17.3125 4.582031 22.167968 7.386719 4.832032 17.304688 2.792969 22.160156-4.585937l12.960938-19.71875v42.144531c0 35.582032-29.863281 64.527344-66.585938 64.527344h-10.316406c-36.714844 0-66.585937-28.945312-66.585937-64.527344v-162.898437c27.847656-7.015625 48.519531-31.773438 48.519531-61.261719zm-97.03125 0c0-17.265625 14.585938-31.3125 32.511719-31.3125 17.929687 0 32.511719 14.046875 32.511719 31.3125 0 17.261719-14.582032 31.3125-32.511719 31.3125-17.925781 0-32.511719-14.050781-32.511719-31.3125zm0 0';
  const HOOK_PATH = new Path2D(HOOK_SVG);
  const SCALE = 0.1;
  const OFFSET = 80;

  // function draw(ctx, location) {
  //     ctx.fillStyle = 'deepskyblue';
  //     ctx.shadowColor = 'dodgerblue';
  //     ctx.shadowBlur = 20;
  //     ctx.save();
  //     ctx.scale(SCALE, SCALE);
  //     ctx.translate(location.x / SCALE - OFFSET, location.y / SCALE - OFFSET);
  //     ctx.fill(HOOK_PATH);
  //     ctx.restore();
  // }

  function drawCircle(ctx, location){

    const asideElement = document.querySelector('aside:first-of-type');
    
    //var offsetWidth = asideElement.offsetWidth + 20;
    const offsetWidth = canvasRef.current.getBoundingClientRect().left;
    const offsetHeight = canvasRef.current.getBoundingClientRect().top - 24;

    const realX = location.x - offsetWidth;
    const realY = location.y - offsetHeight;

    ctx.beginPath();

    // ctx.fillStyle = 'deepskyblue';
    // ctx.shadowColor = 'dodgerblue';
    // ctx.shadowBlur = 20;
    ctx.arc(realX, realY, 10, 0, 2 * Math.PI);

    ctx.strokeStyle = 'blue';
    
    ctx.globalCompositeOperation = "overlay";

    ctx.stroke();

  }


  function drawCross(ctx, location){

    const asideElement = document.querySelector('aside:first-of-type');
    
    //var offsetWidth = asideElement.offsetWidth + 20;
    const offsetWidth = canvasRef.current.getBoundingClientRect().left;
    const offsetHeight = canvasRef.current.getBoundingClientRect().top - 24;

    const realX = location.x - offsetWidth;
    const realY = location.y - offsetHeight;

    ctx.beginPath();

    ctx.moveTo(realX - 10, realY - 10);
    ctx.lineTo(realX + 10, realY + 10);

    ctx.moveTo(realX + 10, realY - 10);
    ctx.lineTo(realX - 10, realY + 10);
    
    ctx.strokeStyle = 'red';
    
    ctx.globalCompositeOperation = "overlay";

    ctx.stroke();

  }

  function handleCanvasClick(e) {
    // const newLocation = { x: e.clientX, y: e.clientY };
    // setLocations([...locations, newLocation]);
    const newLocation = { x: e.clientX, y: e.clientY - 24 };

    if(shapeType == "circle"){
      setCircles([...circles, newLocation]);
      setShapesAdded([...shapesAdded, "circle"]);
    } else if(shapeType == "cross"){
      setCrosses([...crosses, newLocation]);
      setShapesAdded([...shapesAdded, "cross"]);
    }

  }

  function handleClear() {
    setCircles([]);
    setCrosses([]);
    setShapesAdded([]);
  }

  function handleUndo() {

    if(shapesAdded[shapesAdded.length -1] == "circle"){
      setCircles(circles.slice(0, -1));
    } else if(shapesAdded[shapesAdded.length -1] == "cross"){
      setCrosses(crosses.slice(0, -1));
    }

    setShapesAdded(shapesAdded.slice(0, -1));
  
  }

  const mainDivRef = useRef(null);

  const [files, setFile] = useState([]);
  const [message, setMessage] = useState();

  const handleFile = (e) => {
      setMessage("");
      let file = e.target.files;

      for (let i = 0; i < file.length; i++) {
          const fileType = file[i]['type'];
          const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
          if (validImageTypes.includes(fileType)) {
              setFile([...files, file[i]]);
          } else {
              setMessage("only images accepted");
          }

      }
  };

  const removeImage = (i) => {
      setFile(files.filter(x => x.name !== i));
  }


  useEffect(() => {
    const canvas = canvasRef.current;
    const ctx = canvas.getContext('2d');

    ctx.clearRect(0, 0, 500, 320);

    var base_image = new Image();
    // base_image.src = 'https://cdn.vectorstock.com/i/1000x1000/45/00/car-blueprint-vector-7784500.webp';
    base_image.src = 'https://as1.ftcdn.net/v2/jpg/03/17/01/38/1000_F_317013885_I7zxRJfRq2P99eFqNUQX4BCjZao6dKcl.jpg';
    base_image.onload = function(){  
        ctx.drawImage(base_image, 0, 0, base_image.width, base_image.height,     // source rectangle
        0, 0, canvas.width, canvas.height);
    }
    
    circles.forEach((location) => drawCircle(ctx, location));      
    crosses.forEach((location) => drawCross(ctx, location));

  },[circles, crosses]);



  return (
    <div className="mt-10 mx-7">

      <div className="flex flex-row justify-between">
        <div>
          <div className="flex flex-row">
            <h2 className="text-[24px] font-semibold">
              Work Order
            </h2>
            <h2 className="text-[24px]">/#7712</h2>
          </div>
          <div>
            <p>Detailed status of incoming car</p>
          </div>
        </div>
        <button className="flex justify-center items-center w-[128px] h-[40px] border-[1px] border-[#3682F7] rounded-[20px] text-[#3682F7] hover:bg-[#3682F7] hover:text-[#FFFF] text-[15px] mt-[10px]">
          Save
        </button>
      </div>

      <div className="grid grid-cols-12 gap-5 mt-[30px] pt-[20px]">

        <div className="col-span-12">
          
          <div className="grid grid-cols-12 ">
            <div className="col-span-12 md:col-span-7 lg:col-span-7 ml-7">
              <h2 className="text-4xl font-bold mb-4">
                  Dents & Scratches
              </h2>
            </div>

            <div className="col-span-12 md:col-span-5 lg:col-span-5 pl-4">
              <h2 className="text-4xl font-bold mb-4">
                  Images
              </h2>
            </div> 
          </div>         

        </div>

        <div className="col-span-12 md:col-span-7 lg:col-span-7 bg-[#fff] rounded-[25px] py-7 grid justify-items-center">

          <div className="flex flex-row">
            <div className="flex flex-col mr-7">
              
              <button className="rounded text-blue-700 bg-[#F6F6FA] disabled:bg-[#3682F7] disabled:text-[#FFFFFF] p-2 hover:bg-[#3682F7] hover:text-[#FFFF] text-[15px] m-2" disabled={shapeType == "circle" ? true : false} onClick={() => {setShapeType("circle")}} >
                <div>
                  <span className="text-[44px] leading-3">○</span>
                </div>
                <div>
                  <span className="">Dent</span>
                </div>
              </button>

              <button className="rounded text-red-700 bg-[#F6F6FA] disabled:bg-red-700 disabled:text-[#FFFFFF] p-2 hover:bg-[#3682F7] hover:text-[#FFFF] text-[15px] m-2" disabled={shapeType == "cross" ? true : false} onClick={() => {setShapeType("cross")}} >
                <div>
                  <span className="text-[44px] leading-3">x</span>
                </div>
                <div>
                  <span className="">Scratch</span>
                </div>
              </button>

              <button className="rounded text-[#3682F7] bg-[#F6F6FA] p-4 hover:bg-[#3682F7] hover:text-[#FFFF] text-[15px] m-2" onClick={handleUndo}>Undo</button>
              
              <button className="rounded text-[#3682F7] bg-[#F6F6FA] p-4 hover:bg-[#3682F7] hover:text-[#FFFF] text-[15px] m-2" onClick={handleClear}>Clear</button>
              
            </div>
            <canvas
                ref={canvasRef}
                width="500" 
                height="320" 
                style={{ border: "1px solid #F6F6FA"}}
                onClick={handleCanvasClick}
            >
                Your browser does not support the HTML canvas tag.
            </canvas>
            
            
          </div>

        </div>

        <div className="col-span-12 md:col-span-5 lg:col-span-5 bg-[#fff] rounded-[25px] py-7 px-7">
          
          <div className="justify-center items-center bg-[#F6F6FA] px-2">
            <div className="p-3 rounded-md">
                <span className="justify-center items-center bg-white text-[12px] mb-1 text-red-500">{message}</span>
                
                <div className="h-32 w-full overflow-hidden relative shadow-md border-2 items-center rounded-md cursor-pointer   border-gray-400 border-dotted">
                    <input type="file" onChange={handleFile} className="h-full w-full opacity-0 z-10 absolute" multiple="multiple" name="files[]" />
                    <div className="h-full w-full bg-gray-200 absolute z-1 flex justify-center items-center top-0">
                        <div className="flex flex-col">
                            <i className="mdi mdi-folder-open text-[30px] text-gray-400 text-center"></i>
                            <span className="text-[12px]">{`Drag and Drop a file`}</span>
                        </div>
                    </div>
                </div>

                <div className="flex flex-wrap gap-2 mt-2">
                    {files.map((file, key) => {
                        return (
                  
                            <div key={key} className='w-full h-16 flex items-center justify-between rounded p-3 bg-white'>
                                    <div className="flex flex-row items-center gap-2">
                                        <div className="h-12 w-12 ">
                                        <img className="w-full h-full rounded" src={URL.createObjectURL(file)} />
                                        </div>
                                    <span className="truncate w-44">{file.name}</span>
                                    </div>
                                <div onClick={() => { removeImage(file.name) }} className="h-6 w-6 bg-red-700 flex items-center cursor-pointer justify-center rounded-sm text-[#ffffff]">
                                        {/* <i className="mdi mdi-trash-can text-white text-[14px]"></i> */}X
                                    </div>
                              </div>   
                          
                        )
                    })}
                </div>

            </div>
          </div>

        </div>

      </div>
    </div>
  );
}

AddMedia.auth = true;
AddMedia.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};
