import Image from "next/image";

import imagenFilter from "../../assets/img/filter.svg";

import TablaDeDatos from "../../components/Orders/AssignSRS/TablaDeDatos";
import Layout from "../../components/layout";

export default function pantallaTres() {
  return (
    <div className="overflow-y-auto h-screen">
      <div className="mt-[56px] ml-[20px] mr-[20px]">
        <div className="grid grid-cols-12">
          <div className="col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between">
            <div className="text-[24px] md:text-[32px] lg:text-[32px] text-[#000000] ">
              Nueva Orden
            </div>
            <div className="flex flex-row">
              <div className="text-right self-center">
                <span className="text-[14px] md:text-[16px] lg:text-[16px] text-[#000000]">
                  {" "}
                  Juan Camacho
                </span>
                <h1 className="text-[14px] md:text-[16px] lg:text-[16px] text-[#ACB5BD]">
                  ASESOR DE SERVICIO
                </h1>
              </div>
              <div className="w-[52px] h-[52px] md:w-[56px] md:h-[56px] lg:w-[62px] lg:h-[62px] rounded-full bg-[#E4E7EB] ml-[5px]"></div>
            </div>
          </div>
        </div>


        <div className="grid grid-cols-12 gap-3 bg-[#fff] rounded-[25px] mt-[30px] pt-[20px]">
          <div className=" md:col-span-4 lg:col-span-4"></div>
          <div className="col-span-12 md:col-span-4 lg:col-span-4 justify-center items-center mr-[10px] ml-[10px] lg:mr-[10px] lg:ml-[0px]">
            <input
              className="w-full h-[30px] rounded-lg border-[#E4E7EB] border pl-[10px] pr-[10px] "
              placeholder="Búsqueda"
            />
          </div>

          <div className="col-span-12 md:col-span-4 lg:col-span-4 flex">
            <div className="w-[18px] h-[15px] self-center ml-[10px] mr-[10px]">
              <Image
                src={imagenFilter}
                layout="responsive"
                alt="imagenfilter"
              />
            </div>
            <select className="w-full h-[30px] flex justify-center items-center rounded-lg border-[#E4E7EB] border">
              <option value="a" className="text-[12px] font-light">
                NOMBRE DE CLIENTE
              </option>
              <option value="b">b</option>
              <option value="c">c</option>
              <option value="d">d</option>
            </select>
            <button className="w-[35px] h-[30px] flex justify-center items-center text-center rounded-lg border-[#3682F7] hover:bg-[#3682F7] border ml-[10px] mr-[10px] lg:ml-[10px] lg:mr-[20px]">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-5 h-5 text-[#3682F7] hover:text-[#fff]"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
                />
              </svg>
            </button>
          </div>

          <div className="col-span-12 md:col-span-12 lg:col-span-12 mt-[20px]">
            <TablaDeDatos />
          </div>
        </div>
      </div>
    </div>
  );
}
pantallaTres.auth = true;
pantallaTres.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};
