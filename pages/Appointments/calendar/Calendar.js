import React, { useEffect, useState } from "react";
import Layout from "../../../components/layout";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";

import Calendario from "../../../components/Appointments/Calendar/Calendario";
import TableCitasConfir from "../../../components/Appointments/Calendar/tableConfirmados";
import TableCitasNext from "../../../components/Appointments/Calendar/tableCitasNext";
import TableCitasAll from "../../../components/Appointments/Calendar/tableCitasAll";
import TableCitasCancel from "../../../components/Appointments/Calendar/tableCitasCancel";
import CounterConfirmadas from "../../../components/Appointments/Calendar/counterConfirmadas";
import CounterReservas from "../../../components/Appointments/Calendar/counterReservas";
import CounterCancel from "../../../components/Appointments/Calendar/counterCancel";

function pantallaTres() {
  // const { date } = props;
  return (
    <div className="mt-[20px] ml-[20px] mr-[20px]">
      <div className="grid grid-cols-12">
        <div className="col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between">
          <div className="flex flex-row">
            <h2 className="text-[24px] font-semibold">Calendario</h2>
            <h2 className="text-[14px] pt-[10px]">/Citas Hoy</h2>
          </div>
          <div>
            <button className=" w-[101px] h-[30px] bg-[#3682F7] border-[#3682F7] border rounded-[30px] text-[17px] text-[#fff] text-center text-[18px] hover:bg-[#F6F6FA] hover:text-[#3682F7] ">
              NUEVA
            </button>
          </div>
        </div>
      </div>

      <div className="grid grid-cols-12 gap-6">
        <div className="col-span-12 md:col-span-12 lg:col-span-12 mt-[40px] items-center self-center">
          <Calendario />
        </div>
      </div>

      <div className="grid grid-cols-12 gap-6 mt-[25px] md:mt-[35px] lg:mt-[50px]">
        <div className="col-span-12 md:col-span-12 lg:col-span-2">
          <div className="grid grid-cols-12 gap-3">
            <div className="flex flex-col justify-center items-center col-span-4 md:col-span-4 lg:col-span-12 bg-[#fff] shadow-md rounded-[17px] pl-[10px] pt-[10px] pr-[10px] pb-[10px]">
              <div className="bg-[#3682F7] w-[50px] h-[25px] rounded mt-2 flex justify-center shadow-md">
                <div className="bg-[#FFFF] w-[18px] h-[12px] rounded flex justify-center mt-2 ">
                  <p className="text-[#3682F7] text-[12px] font-semibold mt-[-4px]">
                    ↑
                  </p>
                </div>
                <p className="text-[14px] text-[#FFFF] mt-[3px] ml-[4px]">
                  15%
                </p>
              </div>
              <div className="flex flex-col justify-center items-center">
                <p className="text-[#000000] text-[34px] md:text-[38px]  lg:text-[40px] font-medium ">
                  <CounterReservas />
                </p>
                <p className="text-[#000000] text-[15px] font-light mt-[-12px]">
                  Reservas
                </p>
              </div>
            </div>

            <div className="flex flex-col justify-center items-center col-span-4 md:col-span-4 lg:col-span-12 bg-[#fff] shadow-md rounded-[17px] pl-[10px] pt-[10px] pr-[10px] pb-[10px]">
              <div className="bg-[#3682F7] w-[50px] h-[25px] rounded mt-2 flex justify-center shadow-md">
                {" "}
                <div className="bg-[#FFFF] w-[18px] h-[12px] rounded flex justify-center mt-2 ">
                  <p className="text-[#3682F7] text-[12px] font-semibold mt-[-4px]">
                    ↑
                  </p>
                </div>
                <p className="text-[14px] text-[#FFFF] mt-[3px] ml-[4px]">
                  15%
                </p>
              </div>
              <div className="flex flex-col justify-center items-center">
                <p className="text-[#000000] text-[34px] md:text-[38px]  lg:text-[40px] font-medium ">
                  <CounterConfirmadas />
                </p>
                <p className="text-[#000000] text-[15px] font-light mt-[-12px]">
                  Confirmadas
                </p>
              </div>
            </div>

            <div className="flex flex-col justify-center items-center col-span-4 md:col-span-4 lg:col-span-12 bg-[#fff] shadow-md rounded-[17px] pl-[10px] pt-[10px] pr-[10px] pb-[10px]">
              <div className="bg-[#EE002B] w-[50px] h-[25px] rounded mt-2 flex justify-center shadow-md">
                <div className="bg-[#FFFF] w-[18px] h-[12px] rounded flex justify-center mt-2 ">
                  <p className="text-[#EE002B] text-[12px] font-semibold mt-[-4px]">
                    ↑
                  </p>
                </div>
                <p className="text-[14px] text-[#FFFF] mt-[3px] ml-[4px]">
                  15%
                </p>
              </div>
              <div className="flex flex-col justify-center items-center">
                <p className="text-[#000000] text-[34px] md:text-[38px]  lg:text-[40px]  font-medium ">
                  <CounterCancel />
                </p>
                <p className="text-[#000000] text-[15px] font-light mt-[-12px]">
                  Canceladas
                </p>
              </div>
            </div>
          </div>
        </div>

        <div className="col-span-12 md:col-span-12 lg:col-span-10 ">
          <Tabs className="">
            <TabList className="flex flex-row w-full gap-4 md:w-[280px] lg:w-[300px] bg-[#E4E7EB] focus:outline-none rounded-t-lg h-8">
              <Tab className="cursor-pointer flex justify-center items-center text-[12px] focus:text-[#fff] focus:bg-[#3682F7] focus:text-[#fff] px-2">
                Siguientes
              </Tab>
              <Tab className="cursor-pointer text-gray-500 text-[12px] flex justify-center items-center focus:bg-[#3682F7] focus:text-[#fff]  px-2 py-2">
                Todos
              </Tab>
              <Tab className="cursor-pointer text-gray-500 text-[12px] flex justify-center items-center focus:bg-[#3682F7] focus:text-[#fff]  px-2 py-2">
                Confirmados
              </Tab>
              <Tab className="cursor-pointer text-gray-500 text-[12px] flex justify-center items-center focus:bg-[#3682F7] focus:text-[#fff]  px-2 py-2">
                Cancelados
              </Tab>
            </TabList>

            <TabPanel>
              <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                  <TableCitasNext />
                </div>
              </div>
            </TabPanel>
            <TabPanel>
              <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                  <TableCitasAll />
                </div>
              </div>
            </TabPanel>
            <TabPanel>
              <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                  <TableCitasConfir />
                </div>
              </div>
            </TabPanel>
            <TabPanel>
              <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                  <TableCitasCancel />
                </div>
              </div>
            </TabPanel>
          </Tabs>
        </div>
      </div>
    </div>
  );
}
pantallaTres.auth = true;
pantallaTres.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};

export default pantallaTres;
