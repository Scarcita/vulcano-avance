import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";



export default function ModalEditUser(props) {
    const { 
        row, 
        name, 
        username, 
        role, 
        reloadUsers,
        setReloadUsers
    } = props;
    const [isLoading, setIsLoading] = useState(false)
    const [showEditModal, setShowEditModal] = useState(false);
    const [nameForm, setNameForm] = useState(name)
    const [userNameForm, setUserNameForm] = useState(username)
    const [roleForm, setRoleForm] = useState(role)
    const [passwordForm, setPasswordForm] = useState('')
    const [passwordRepeatForm, setPasswordRepeatForm] = useState('')
    const [phoneForm, setPhoneForm] = useState('')
    const [emailForm, setEmailForm] = useState('')

    const actualizarDatos = async (formData) => {

        setIsLoading(true)
        var data = new FormData();

        data.append("name", nameForm);
        data.append("username", userNameForm);
        data.append("role", roleForm);
        data.append("password", passwordForm);
        data.append("passwordrepeat", passwordRepeatForm);
        data.append("phone", phoneForm);
        data.append("email", emailForm);

        fetch("https://slogan.com.bo/vulcano/users/editMobile/" + row.id , {
          method: 'POST',
          body: data,
        })
          .then(response => response.json())
          .then(data => {
            console.log('VALOR ENDPOINTS EDIT: ', data);
            setIsLoading(false)
            if (data.status) { 
                setReloadUsers(!reloadUsers)
                setShowEditModal(false)
                console.log('edit endpoint: ' + data.status);
            } else {
                console.error(data.error)
            }
        })
    
      }

    const validationSchema = Yup.object().shape({
        name: Yup.string()
        .required('name is required'),

        username: Yup.string()
        .required("username is required"),
        //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

        role: Yup.string()
        .required('role is required'),
        //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

        password: Yup.string()
        .required('password is required'),
        //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

        passwordrepeat: Yup.string()
        .required('passwordrepeat is required'),
        //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

        phone: Yup.string()
        .required('phone is required'),
        //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

        email: Yup.string()
        .required('email is required'),
        //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

        });

    const formOptions = { resolver: yupResolver(validationSchema) };
    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
        console.log('onSubmit data clients:');
        console.log(data);
        actualizarDatos(data);
    }

    return (
        <div>
            <form onSubmit={handleSubmit(onSubmit)}>

                <div className="mt-[20px] grid grid-cols-12 gap-4">

                    <div className='col-span-12 md:col-span-12 lg:col-span-6 '>
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-left'>name</p>
                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                            type="text"
                            name="name"
                            {...register('name')}
                            value={nameForm}
                            onChange={(e) => {
                                setNameForm(e.target.value)
                            }}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.name?.message}</div>
                    </div>

                    <div className='col-span-12 md:col-span-12 lg:col-span-6'>
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-left'>Username</p>
                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                            type="text"
                            name="username"
                            {...register('username')}
                            value={userNameForm}
                            onChange={(e) => setUserNameForm(e.target.value)}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.username?.message}</div>
                    </div>
                    <div className='col-span-12 md:col-span-6 lg:col-span-6'>
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-left'>Password</p>
                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px]"
                            type="text"
                            name="password"
                            {...register('password')}
                            value={passwordForm}
                            onChange={(e) => setPasswordForm(e.target.value)}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.password?.message}</div>
                    </div>
                    <div className='col-span-12 md:col-span-6 lg:col-span-6'>
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-left'>Password Repeat</p>
                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px]"
                            type="text"
                            name="passwordrepeat"
                            {...register('passwordrepeat')}
                            value={passwordRepeatForm}
                            onChange={(e) => setPasswordRepeatForm(e.target.value)}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.passwordrepeat?.message}</div>
                    </div>
                    <div className="col-span-12 md:col-span-6 lg:col-span-6">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-left'>Email</p>
                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                            type="email"
                            name="email"
                            {...register('email')}
                            value={emailForm}
                            onChange={(e) => setEmailForm(e.target.value)}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.email?.message}</div>
                    </div>
                    <div className="col-span-12 md:col-span-6 lg:col-span-6">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-left'>Phone</p>
                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                            type="text"
                            name="phone"
                            {...register('phone')}
                            value={phoneForm}
                            onChange={(e) => setPhoneForm(e.target.value)}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.phone?.message}</div>
                    </div>

                    <div className="col-span-12 md:col-span-6 lg:col-span-6 ">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start text-left'>Role</p>
                        <select
                            name="role"
                            {...register('role')}
                            value={roleForm}
                            onChange={(e) => {
                                setRoleForm(e.target.value)
                            }}
                            className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                        >
                            <option value="Administrator">Administrator</option>
                            <option value="Shop Foreman">Shop Foreman</option>
                            <option value="Mechanic">Mechanic</option>
                            <option value="Inventory">Inventory</option>
                            <option value="Invoicing">Invoicing</option>
                            <option value="Service Advisor">Service Advisor</option>
                        </select>
                        <div className="text-[14px] text-[#FF0000]">{errors.role?.message}</div>
                    </div>
                </div>
                <div className="flex flex-row justify-between mt-[20px]">

                    <div>
                        <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                    </div>

                    <div>
                        <button
                            className="w-[72px] h-[30px] text-[#FFFFFF] bg-[#000] text-[12px] rounded-[9px] text-center items-center hover:bg-transparent hover:border-[#000] hover:border hover:text-[#000] justify-center mr-[10px]"
                            onClick={() =>
                                setShowEditModal(false)
                            }
                            disabled={isLoading}
                        >
                            Cancel
                        </button>
                        <button
                            className="w-[72px] h-[30px] text-[#FFFFFF] bg-[#3682F7] text-[12px] rounded-[9px] text-center items-center hover:bg-transparent hover:border-[#3682F7] hover:border hover:text-[#3682F7] justify-center"
                            type="submit"
                            disabled={isLoading}
                        >
                            {isLoading ? <Dots className='m-auto' size={7} color={'#fff'}></Dots> : 'Update'}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    );
}