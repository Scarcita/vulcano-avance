import React, { useEffect, useState } from 'react';
// import ModalProducto from "./ModalProducto";

const ListaProductos = (props) => {
    const { dataColumnaDerecha, removeFromCart } = props;

    const [precioTotal, setPrecioTotal] = useState(0)
    const { precioSugerido, setPrecioSugerido } = props;

    useEffect(() => {
        console.log("Precio Sugerido Lista Productos: " + precioSugerido)

    }, [precioSugerido])

    useEffect(() => {
        const calculatetotal = () => {
            var temp_total = 0;
            dataColumnaDerecha.forEach(element => {
                temp_total += parseInt(element.price);
            });
            setPrecioTotal(temp_total);
        }
        calculatetotal();
    }, [dataColumnaDerecha])





    return (
        <>
            <div className=" justify-center ">
                <div className=" flex-col h-[210px] overflow-auto rounded-2xl" >
                    <div className="bg-[#FFFF]  pl-5 pr-5 rounded-2xl shadow-md flex flex-col flex rounded">
                        {/* {Object.values(dataColumnaDerecha).map((item, index) => {
                            console.log("DATA DERECHA : " + Object.entries(dataColumnaDerecha[0]))
                            return (
                                <div key={Object} className=" pl-3 pr-5 mt-3" >
                                    <div className="flex felx-row justify-between items-center">
                                        <div>
                                            <p className="text-[20.9px] font-bold">
                                                {item.name}
                                            </p>
                                        </div>
                                        <div className="w-[7.45px] h-[26.08px]">
                                            <ModalProducto
                                                index={index}
                                                removeFromCart={removeFromCart}
                                                setPrecioTotal={setPrecioTotal}
                                            />
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            <p className="font-light text-[14px]">
                                                {item.code}
                                            </p>
                                        </div>
                                        <div className="grid grid-cols-2">
                                            <div>
                                                <p className="text-[24.31px] font-bold text-[#3682F7]">
                                                    Bs. {parseInt(item.price)}
                                                </p>
                                            </div>
                                            <div className="flex justify-center items-center">
                                                <div className="px-2">
                                                    <p className="font-medium text-[14.9px] text-[#A5A5A5]">x</p>
                                                </div>
                                                <div>
                                                    <p className="text-[18px] font-light text-[#A5A5A5]">
                                                        {item.created_by}
                                                    </p>
                                                </div>
                                                <div className="px-2">
                                                    <p className="font-medium text-[14.9px] text-[#A5A5A5]">
                                                        ud/pzas
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr></hr>
                                </div>
                            );
                        })} */}
                    </div>
                </div>

                <div className="lg:flex-col md:flex-col mt-5">
                    <div>
                        <p className="font-light text-[18px]">
                            Cantidad de ud/pzas: 
                            {/* {dataColumnaDerecha.length} */}
                        </p>
                    </div>
                    <div className="grid lg:grid-cols-2 md:grid md:grid-cols-2 grid grid-cols-2 ">
                        <div>
                            <p className="text-[30px] font-bold text-[#3682F7]">
                                Total
                            </p>
                        </div>
                        <div className='w-18'>
                            <p className="text-[30px] font-bold text-[#3682F7]">
                                Bs. {parseInt(precioTotal) + parseInt(precioSugerido)}
                            </p>
                        </div>
                    </div>
                    <div className="grid grid-cols-12" >
                        <button
                            type={'submit'}
                            className='col-span-12 md:col-span-12 lg:col-end-span-12 h-[45px] bg-[#3682F7] border-[#3682F7] border-2 rounded-lg text-[25.45px] text-[#fff]'>
                            Guardar
                        </button>
                    </div>
                </div>
            </div>
        </>
    )
}
export default ListaProductos;
