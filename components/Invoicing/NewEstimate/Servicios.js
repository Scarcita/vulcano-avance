import React, { useEffect, useState } from 'react';
import { ReactSearchAutocomplete } from 'react-search-autocomplete'

import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'
import ModalServicios from './ModalServicios';

export default function Servicios(props) {

  const { servicesList, setServicesList } = props;
  const [isLoading, setIsLoading] = useState(false);
  const [brands, setBrands] = useState([]);
  const [name, setName] = useState([]);

  const [modal, setModal] = useState(false);

  const { selectedItem, setSelectedItem } = props;

  const getDatos = () => {
    setIsLoading(true);

    fetch('https://slogan.com.bo/vulcano/services/all')
      .then(response => response.json())
      .then(dataResponse => {
        if (dataResponse.status) {

          setBrands(Object.values(dataResponse.data));
          console.log(Object.values(dataResponse.data));

        } else {
          console.error(dataResponse.error)
        }
        setIsLoading(false)
      });
  }
  useEffect(() => {
    getDatos();
  }, [])


  ////////////////// AUTOCOMPLETE ////////////////////
  const handleinput = (e) => {
    console.log("input: " + e.target.value);
    setName(e.target.value);
  };

  const handleOnSearch = (string, results) => {
    console.log(string, results);
  };


  const handleOnSelect = (item) => {
    console.log(item);
    console.log("submit: " + item.name);

    // if (typeof item.name === 'string' && item.name.length === 0) {
    //   alert('string is empty');
    //   return false;
    // }
    // if (servicesList.some(e => e.id === item.id)) {
    //   /* vendors contains the element we're looking for */
    //   alert('item is already on the list');
    //   return false;
    // }
    setSelectedItem(item)
    setModal(true)
  }

  const formatResult = (item) => {
    return (
      <>
        <div className='grid grid-cols-12 flex justify-center items-center'>
          <span className='col-span-10 md:col-span-10 lg:col-span-10'>
            {item.name}
          </span>
        </div>
      </>
    );
  };

  return (
    <div>
      <div className="grid grid-cols-12 gap-3 flex flex-row justify-betweens">
        <div className=" col-span-12 md:col-span-12 lg:col-span-6">
          <p className='text-[18px] text-[#3682F7] self-center'>
            Servicios
          </p>
        </div>
        <div className=" col-span-12 md:col-12 lg:col-span-6">
          <div className='mb-4 justify-center items-center'>
            {isLoading ?
              <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                <Spinner color="#3682F7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
              </div>
              :
              modal == true ?
                <ModalServicios
                  selectedItem={selectedItem}
                  servicesList={servicesList}
                  setServicesList={setServicesList}
                  setModal={setModal}
                  modal={modal}
                />
                :
                <ReactSearchAutocomplete
                  items={brands}
                  fuseOptions={{ threshold: 0 }}
                  onSearch={handleOnSearch}
                  value={name}
                  handleinput={handleinput}
                  inputSearchString={name}
                  onSelect={handleOnSelect}
                  formatResult={formatResult}
                  showItemsOnFocus={true}
                  styling={{
                    height: "34px",
                    border: "1px solid lightgray ",
                    borderRadius: "10px",
                    borderColor: "lightgray",
                    backgroundColor: "white",
                    boxShadow: "12px",
                    hoverBackgroundColor: "white",
                    color: "gray",
                    fontSize: "12px",
                    iconColor: "gray",
                    lineColor: "lightgray",
                    placeholderColor: "gray",
                    clearIconMargin: "3px 8px 0 0",
                    cursor: "pointer",
                    zIndex: 8,
                  }}
                />
            }
          </div>
        </div>
      </div>
    </div>
  );
};
