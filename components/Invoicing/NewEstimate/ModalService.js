import React, { useEffect, useState } from 'react';

function ModalService(props) {

    const { id, dataPlate, data } = props;
    const [showModal, setShowModal] = useState(false);
    const [description, setDescription] = useState("");

    const agregarServicio = (item) => {
        data = item
    }


    return (
        <>
            <div className=''>
                <button onClick={() => setShowModal(true)}>
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-5 h-5 text-[#3682F7]">
  <path strokeLinecap="round" strokeLinejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10" />
</svg>
                </button>
            </div>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                       
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">
                                <div>
                                    <h4 className="text-lg font-medium text-gray-800">
                                        Edit payment Method
                                    </h4>

                                </div>

                                <div className="mt-[20px] grid grid-cols-12 gap-4">
                                    <div className='col-span-12 md:col-span-12 lg:col-span-12 '>
                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Descripción</p>
                                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                            type="text"
                                            value={dataPlate}
                                            onChange={(e) => dataPlate(e)}
                                        />
                                    </div>

                                    <div className='col-span-12 md:col-span-12 lg:col-span-12 mt-[10px]'>
                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Credit/debit card number</p>
                                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                            type="text"
                                            value={description}
                                            onChange={(e) => setDescription(e)}
                                        />
                                    </div>

                                    <div className='col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]'>
                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Expiration month and year</p>
                                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px]"
                                            type="text"
                                            value={description}
                                            onChange={(e) => setDescription(e)}
                                        />
                                    </div>
                                    <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                        <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>CVC</p>
                                        <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                            type="text"
                                            value={description}
                                            onChange={(e) => setDescription(e)}
                                        />
                                    </div>
                                </div>
                                <div className="flex flex-row justify-between mt-[20px]">

                                    <div>
                                        <h1 className="text-[12px] mt-[10px]">
                                            This field is mandatory
                                        </h1>
                                    </div>

                                    <div>
                                        <button
                                            className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                            onClick={() =>
                                                setShowModal(false)
                                            }
                                        >
                                            Cancel
                                        </button>
                                        <button
                                            data={data}
                                            className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                                            onClick={() => {
                                                agregarServicio(row),
                                                    setShowModal(false)
                                            }}
                                        >
                                            Update
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}

export default ModalService;
