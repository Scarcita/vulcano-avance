import React, { useEffect, useState } from 'react';
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import Chart from "chart.js";

export default function BarChartWeek() {
  const [isLoading, setIsLoading] = useState(true)
  const [appointmentsCreated, setAppointmentsCreated] = useState([]);

  useEffect(() => {

      setIsLoading(true)

      fetch('https://slogan.com.bo/vulcano/appointments/dashboard')
          .then(response => response.json())
          .then(data => {
              if (data.status) {
                  var temp = [];
                  Object.values(data.data).map((result) =>{
                      temp.push(result);
                  })
  
                  setAppointmentsCreated(temp)
                  
              } else {
                  console.error(data.error)
              }
              setIsLoading(false)
          })

  }, [])

    React.useEffect(() => {
        let config = {
          type: "bar",
          data: {
            labels: [
              "Created",
              "Confirmed",
              "Cancelled",
    
            ],
            datasets: [
              {
                label: new Date().getMonth(),
                backgroundColor: "#3682F7",
                //borderColor: "#4a5568",
                data: [
                  appointmentsCreated[0],
                  appointmentsCreated[1],
                  appointmentsCreated[2]
                 ],
                fill: false,
                barThickness: 12,
              },
            ],
          },
          options: {
            maintainAspectRatio: false,
            responsive: true,
            title: {
              display: false,
              text: "Orders Chart",
            },
            tooltips: {
              mode: "index",
              intersect: false,
            },
            hover: {
              mode: "nearest",
              intersect: true,
            },
            legend: {
              labels: {
                fontColor: "rgba(0,0,0,.4)",
                color: "#3682F7"
              },
              align: "end",
              position: "bottom",
            },
            scales: {
              xAxes: [
                {
                  display: false,
                  scaleLabel: {
                    display: true,
                    labelString: "Month",
                  },
                  gridLines: {
                    borderDash: [2],
                    borderDashOffset: [2],
                    color: "rgba(33, 37, 41, 0.3)",
                    zeroLineColor: "rgba(33, 37, 41, 0.3)",
                    zeroLineBorderDash: [2],
                    zeroLineBorderDashOffset: [2],
                  },
                },
              ],
              yAxes: [
                {
                  display: true,
                  scaleLabel: {
                    display: false,
                    labelString: "Value",
                  },
                  gridLines: {
                    borderDash: [2],
                    drawBorder: false,
                    borderDashOffset: [2],
                    color: "rgba(33, 37, 41, 0.2)",
                    zeroLineColor: "rgba(33, 37, 41, 0.15)",
                    zeroLineBorderDash: [2],
                    zeroLineBorderDashOffset: [2],
                  },
                },
              ],
            },
          },
        };
    
        let ctx = document.getElementById("bar-chartMonth").getContext("2d");
        window.myBar = new Chart(ctx, config);
      }, []);
      return (
          <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-md rounded-md pt-[15px]">
              <div className="relative h-[250px]">
                <canvas id="bar-chartMonth"></canvas>
              </div>
          </div>
      );
    }
    
    
    