import React, { useEffect, useState } from 'react';
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import Chart from "chart.js";

export default function BarChartCitasYear() {
  const [isLoading, setIsLoading] = useState(true)
  const [appointmentsCreated, setAppointmentsCreated] = useState([]);

  useEffect(() => {

      setIsLoading(true)

      fetch('https://slogan.com.bo/vulcano/appointments/dashboard')
          .then(response => response.json())
          .then(data => {
              if (data.status) {
                  var temp = [];
                  Object.values(data.data).map((result) =>{
                      temp.push(result);
                  })
  
                  setAppointmentsCreated(temp)
                  
              } else {
                  console.error(data.error)
              }
              setIsLoading(false)
          })

  }, [])

    React.useEffect(() => {
        let config = {
          type: "bar",
          data: {
            labels: [
              "Jan",
              "Feb",
              "Mar",
              "Apr",
              "May",
              "Jun",
              "Jul",
              "Ago",
              "Sep",
              "Oct",
              "Nov",
              "Dec",
    
            ],
            datasets: [
              {
                label: new Date().getFullYear(),
                backgroundColor: "#3682F7",
                //borderColor: "#4a5568",
                data: [
                  appointmentsCreated[9],
                  appointmentsCreated[10],
                  appointmentsCreated[11],
                  appointmentsCreated[12],
                  appointmentsCreated[13],
                  appointmentsCreated[14],
                  appointmentsCreated[15],
                  appointmentsCreated[16],
                  appointmentsCreated[17],
                  appointmentsCreated[18],
                  appointmentsCreated[19],
                  appointmentsCreated[20],

                 ],
                fill: false,
                barThickness: 12,
              },
            ],
          },
          options: {
            maintainAspectRatio: false,
            responsive: true,
            title: {
              display: false,
              text: "Orders Chart",
            },
            tooltips: {
              mode: "index",
              intersect: false,
            },
            hover: {
              mode: "nearest",
              intersect: true,
            },
            legend: {
              labels: {
                fontColor: "rgba(0,0,0,.4)",
                color: "#3682F7"
              },
              align: "end",
              position: "bottom",
            },
            scales: {
              xAxes: [
                {
                  display: false,
                  scaleLabel: {
                    display: true,
                    labelString: "Month",
                  },
                  gridLines: {
                    borderDash: [2],
                    borderDashOffset: [2],
                    color: "rgba(33, 37, 41, 0.3)",
                    zeroLineColor: "rgba(33, 37, 41, 0.3)",
                    zeroLineBorderDash: [2],
                    zeroLineBorderDashOffset: [2],
                  },
                },
              ],
              yAxes: [
                {
                  display: true,
                  scaleLabel: {
                    display: false,
                    labelString: "Value",
                  },
                  gridLines: {
                    borderDash: [2],
                    drawBorder: false,
                    borderDashOffset: [2],
                    color: "rgba(33, 37, 41, 0.2)",
                    zeroLineColor: "rgba(33, 37, 41, 0.15)",
                    zeroLineBorderDash: [2],
                    zeroLineBorderDashOffset: [2],
                  },
                },
              ],
            },
          },
        };
    
        let ctx = document.getElementById("bar-chartYear").getContext("2d");
        window.myBar = new Chart(ctx, config);
      }, []);
      
      return (
        // isLoading ?
        //     <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
        //         <Spinner color="#3682F7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
        //     </div>
        //     :
          <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-md rounded-md pt-[15px]">
              <div className="relative h-[260px]">
                <canvas id="bar-chartYear"></canvas>
              </div>
          </div>
      );
    }
    
    
    