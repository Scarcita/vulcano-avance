import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import React, { useEffect, useState } from 'react';
import Link from 'next/link'


export default function ResponsiveCalendar(props) {
    
    const { showDots, anterior, centro, posterior, gap } = props;

    return (
        <>
            {
                showDots ?
                    <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                        <Spinner color="#3682F7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                    </div>
                    :
                    <div className="flex flex-row gap-2 justify-center items-center grid grid-cols-12">
                        <div className=" col-span-5 md:col-span-5 lg:col-span-5 flex flex-row justify-center items-center gap-9">
                            {anterior.map(item => (
                                <Link
                                    href={"http://localhost:3000/calendar/" + item.fullDate}>
                                    <div className="flex flex-col justify-center items-center cursor-pointer border-[#3682F7] border-[1px] rounded-[14px] w-[72px] h-[72px] gap-2">
                                        <p className="text-[12px] mt-[02px] font-semibold ">
                                            {item.weekDay}
                                        </p>
                                        <p className="text-[28px] mt-[-15px] font-bold"
                                            id={item.id}
                                        >
                                            {item.dateNumber}
                                        </p>
                                        <p className="text-[700] text-[16px] mt-[-14px] font-bold ">
                                            {item.monthName}
                                        </p>
                                    </div>
                                </Link>
                            ))}
                        </div>
                        <div className="flex justify-center items-center col-span-2 md:col-span-2 lg:col-span-2 ">
                            {centro.map(item => (
                                <div className="flex flex-col justify-center items-center bg-[#FFFFFF] w-[88px] h-[92px] rounded-[14px] gap-1">
                                    <p className="text-[#FF0000] text-[14px] mt-[02px] font-semibold ">
                                        {item.weekDay}
                                    </p>
                                    <p className="text-[37px] mt-[-15px] font-bold ">
                                        {item.dateObject.getDate()}
                                    </p>
                                    <p className="text-[700] text-[18px] mt-[-14px] font-bold ">
                                        {item.monthName}
                                    </p>
                                </div>
                            ))}
                        </div>
                        <div className="flex flex-row gap-9 justify-center items-center col-span-5 md:col-span-5 lg:col-span-5">
                            {posterior.map(item => (
                                <Link
                                    href={"http://localhost:3000/calendar/" + item.fullDate}>
                                    <div className="flex flex-col justify-center items-center cursor-pointer border-[#3682F7] border-[1px] rounded-[14px] w-[72px] h-[72px] gap-2">
                                        <p className="text-[12px] mt-[02px] font-semibold ">
                                            {item.weekDay}
                                        </p>
                                        <p className="text-[28px] mt-[-15px] font-bold"
                                            id={item.id}
                                        >
                                            {item.dateNumber}
                                        </p>
                                        <p className="text-[700] text-[16px] mt-[-14px] font-bold ">
                                            {item.monthName}
                                        </p>
                                    </div>
                                </Link>
                            ))}
                        </div>
                    </div>
            }
        </>
    )
}