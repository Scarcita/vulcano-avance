import React, { useEffect, useState } from "react";

function DetallesCitas(props) {
  const { setDetallesCita } = props;

  const [date, setDate] = useState("");
  const [timeStart, setTimeStart] = useState("08:00");
  const [timeEnd, setTimeEnd] = useState("");

  const [timeEndLocked, setTimeEndLocked] = useState(true);

  useEffect(() => {
    const setTimeEndBasedOnTimeStart = () => {
      let hourStart = parseInt(timeStart.split(":")[0]);
      let minuteStart = parseInt(timeStart.split(":")[1]);

      let hourEnd = hourStart;
      let minuteEnd = minuteStart + 15;

      if (minuteEnd >= 60) {
        hourEnd = hourEnd + 1;
        minuteEnd = minuteEnd - 60;
      }

      if (hourEnd >= 24) {
        hourEnd = hourEnd - 24;
      }

      if (minuteEnd < 10) {
        minuteEnd = "0" + minuteEnd;
      }

      if (hourEnd < 10) {
        hourEnd = "0" + hourEnd;
      }

      setTimeEnd(hourEnd + ":" + minuteEnd);
    };
    setTimeEndBasedOnTimeStart();
  }, [timeStart]);

  const addTimeToTimeEnd = (timeToAdd) => {
    let hours = parseInt(timeEnd.substring(0, 2));
    let minutes = parseInt(timeEnd.substring(3, 5));

    minutes += timeToAdd;

    if (minutes >= 60) {
      hours += 1;
      minutes -= 60;
    }

    if (hours < 10) {
      hours = "0" + hours;
    }

    if (minutes < 10) {
      minutes = "0" + minutes;
    }

    setTimeEnd(hours + ":" + minutes);
  };

  useEffect(() => {
    setDetallesCita({
      fecha: date,
      horaInicio: timeStart,
      horaFin: timeEnd,
    });
  }, [date, timeStart, timeEnd, setDetallesCita]);

  return (
    // CONTENEDOR PRINCIPAL
    <div
      className="
    w-full h-full flex flex-col justify-around items-center
    "
    >
      {/* CABECERA */}
      <div
        className="
        w-full h-14 flex flex-row justify-start items-center
      "
      >
        <p
          className="
          text-lg font-bold
        "
        >
          Detalles de cita
        </p>
      </div>

      {/* CUERPO */}
      <div
        className="
        w-full h-full flex flex-col justify-center items-center p-4 bg-[#fff] rounded-lg shadow-lg rounded-lg
        {/*DESKTOP*/}
        lg:flex-row
      "
      >
        {/* FECHA Y HORA */}
        <div
          className="
            w-full flex flex-col justify-around items-center
            {/*TABLET*/}
            md:flex-row
            {/*DESKTOP*/}
            md:w-4/6
            "
        >
          {/* FECHA */}
          <div
            className="
            w-full flex flex-col justify-center items-center            
            {/*TABLET*/}
            lg:w-1/2
            "
          >
            <label
              className="
                w-full self-start text-s m text-[#A5A5A5] font-light mb-2
                "
            >
              Fecha
            </label>

            <div
              className="
                w-full h-10 flex flex-col justify-center items-center
                "
            >
              <input
                className="
                    w-full h-10 text-md text-center font-normal border-[#D3D3D3] border-2 rounded-lg pt-1 outline-none
                    "
                type="date"
                value={date}
                onChange={(e) => {
                  setDate(e.target.value);
                }}
              />
            </div>
          </div>

          {/* HORA */}
          <div
            className="
            w-full flex flex-row justify-around items-center m-2
            {/*TABLET*/}
            md:w-1/2
          "
          >
            {/* HORA INICIO */}
            <div
              className="
            w-1/2 flex flex-col justify-center items-center
            "
            >
              <label
                className="
                w-full self-start text-md text-[#A5A5A5] font-light mb-2
                "
              >
                Hora Inicio
              </label>
              <div
                className="
                w-full h-10 flex flex-col justify-center items-center
                "
              >
                <input
                  className="
                    w-full h-10 text-md text-center font-normal border-[#D3D3D3] border-2 rounded-lg pt-1 outline-none
                    "
                  type="time"
                  value={timeStart}
                  onChange={(e) => {
                    setTimeStart(e.target.value);
                  }}
                />
              </div>
            </div>
            {/* HORA FIN */}
            <div
              className="
            w-1/2 flex flex-col justify-center items-center ml-2
            "
            >
              <div
                className="
                    w-full flex flex-row justify-between items-center
                    "
              >
                <label
                  className="
                w-full self-start text-s m text-[#A5A5A5] font-light mb-2
                "
                >
                  Hora Fin
                </label>
                <div
                  onClick={() => {
                    setTimeEndLocked(!timeEndLocked);
                  }}
                >
                  {timeEndLocked == false ? (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      fill="currentColor"
                      className="
                    text-[#D3D3D3] w-[19px] h-[18px] cursor-pointer
                    {/*HOVER*/}
                    hover:text-[#000]
                    "
                    >
                      <path d="M18 1.5c2.9 0 5.25 2.35 5.25 5.25v3.75a.75.75 0 01-1.5 0V6.75a3.75 3.75 0 10-7.5 0v3a3 3 0 013 3v6.75a3 3 0 01-3 3H3.75a3 3 0 01-3-3v-6.75a3 3 0 013-3h9v-3c0-2.9 2.35-5.25 5.25-5.25z" />
                    </svg>
                  ) : (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      fill="currentColor"
                      className="
                    text-[#D3D3D3] w-[19px] h-[18px] cursor-pointer
                    {/*HOVER*/}
                    hover:text-[#000] 
                    "
                    >
                      <path
                        fillRule="evenodd"
                        d="M12 1.5a5.25 5.25 0 00-5.25 5.25v3a3 3 0 00-3 3v6.75a3 3 0 003 3h10.5a3 3 0 003-3v-6.75a3 3 0 00-3-3v-3c0-2.9-2.35-5.25-5.25-5.25zm3.75 8.25v-3a3.75 3.75 0 10-7.5 0v3h7.5z"
                        clipRule="evenodd"
                      />
                    </svg>
                  )}
                </div>
              </div>
              <div
                className="
                w-full h-10 flex flex-col justify-center items-center
                "
              >
                <input
                  className={`
                    w-full h-10 text-md text-center font-normal border-2 rounded-lg pt-1 outline-none
                    ${
                      timeEndLocked
                        ? "cursor-not-allowed"
                        : "border-[#D3D3D3] cursor-pointer"
                    }
                    `}
                  type="time"
                  value={timeEnd}
                  readOnly={timeEndLocked}
                  onChange={(e) => {
                    setTimeEnd(e.target.value);
                  }}
                />
              </div>
            </div>
          </div>
        </div>

        {/* BOTONES */}
        <div
          className="
        w-fit h-full flex flex-row justify-around items-center
        "
        >
          <div
            className="
            w-16 h-10 flex flex-row justify-center items-center text-center text-[#fff] bg-[#3682F7] border-[#3682F7] border-2 rounded-full cursor-pointer mx-2
            {/*DESKTOP*/}
            lg:h-1/2
            {/*HOVER*/}
            hover:bg-[#fff] hover:text-[#3682F7]
            "
            onClick={() => {
              addTimeToTimeEnd(15);
            }}
          >
            <p
              className="
              text-sm font-normal
              {/*DESKTOP*/}
              lg:text-lg
              "
            >
              15
            </p>
            <p
              className="
              text-sm font-normal
              {/*DESKTOP*/}
              lg:text-lg
              "
            >
              Min
            </p>
          </div>
          <div
            className="
            w-16 h-10 flex flex-row justify-center items-center text-center text-[#fff] bg-[#3682F7] border-[#3682F7] border-2 rounded-full cursor-pointer mx-2
            {/*DESKTOP*/}
            lg:h-1/2
            {/*HOVER*/}
            hover:bg-[#fff] hover:text-[#3682F7]
            "
            onClick={() => {
              addTimeToTimeEnd(30);
            }}
          >
            <p
              className="
              text-sm font-normal
              {/*DESKTOP*/}
              lg:text-lg
              "
            >
              30
            </p>
            <p
              className="
              text-sm font-normal
              {/*DESKTOP*/}
              lg:text-lg
              "
            >
              Min
            </p>
          </div>
          <div
            className="
            w-16 h-10 flex flex-row justify-center items-center text-center text-[#fff] bg-[#3682F7] border-[#3682F7] border-2 rounded-full cursor-pointer mx-2
            {/*DESKTOP*/}
            lg:h-1/2
            {/*HOVER*/}
            hover:bg-[#fff] hover:text-[#3682F7]
            "
            onClick={() => {
              addTimeToTimeEnd(60);
            }}
          >
            <p
              className="
              text-sm font-normal
              {/*DESKTOP*/}
              lg:text-lg
              "
            >
              1
            </p>
            <p
              className="
              text-sm font-normal
              {/*DESKTOP*/}
              lg:text-lg
              "
            >
              Hr
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
export default DetallesCitas;
