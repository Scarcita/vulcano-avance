import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import React, { useEffect, useState } from 'react';


export default function ProductsCostSum() {
    const [isLoading, setIsLoading] = useState(true)
    const [productsCostSum, setProductsCostSum] = useState([]);

    
    useEffect(() => {

        setIsLoading(true)

        fetch('https://slogan.com.bo/vulcano/products/dashboard')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log(data.data);
                    var temp = [];
                    Object.values(data.data).map((result) =>{
                        temp.push(result);
                    })
    
                    setProductsCostSum(temp[1])
                    console.log(temp);
                    
                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

    }, [])
    return (

        isLoading ?
            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                <Spinner color="#3682F7" size={14} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
            <div>{productsCostSum}</div>
        )
}