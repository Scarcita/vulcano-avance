import React, { useEffect, useState } from "react";
import Image from "next/image";
import Spinner from "react-activity/dist/Spinner";
import "react-activity/dist/Spinner.css";

import ImgMedidor from "../../../public/Desktop8/medidor.svg";
import Img1904 from "../../../public/Desktop8/1904.svg";

export default function ListLastOuts() {
    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([]);

    
    useEffect(() => {

        setIsLoading(true)

        fetch('https://slogan.com.bo/vulcano/products/dashboard')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log(data.data);
                    var temp = [];
                    Object.values(data.data).map((result) =>{
                        temp.push(result);
                    })
    
                    setData(temp[9])
                    console.log(temp);
                    
                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

    }, [])

  return  (
    isLoading ?
            <div className='flex justify-center items-center self-center mt-[30px]'  >
                <Spinner color="#3682F7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
      <div>
        <div style={{ maxHeight: "282px", overflowY: "auto" }}>
          <Table data={data} />
        </div>
      </div>
  );
}

const Table = (props) => {
  const { data } = props;

  return (
    <div className="grid grid-cols-12">
      <table
        className={`col-span-12 md:col-span-12 lg:col-span-12 h-[282px] bg-[#FFFFFF] rounded-[24px]`}
      >
        <tbody>
           <div className="grid grid-cols-1 divide-y pl-3 pr-3">
              {data.map(row => <TableRow key={data.id} row={row} />)}
          </div>
        </tbody>
      </table>
    </div>
  );
};

class TableRow extends React.Component {
  render() {
    let row = this.props.row;

    return (
      <div className="flex flex-row pl-[15px]">
        <div className="w-[54px] h-[54px] bg-[#F6F6FA] rounded-full items-center self-center text-center m-2">
          {row.type === "medidor" ? (
            <Image
              className="items-center self-center text-center"
              src={ImgMedidor}
              layout="fixed"
              alt="ImgMedidor"
              width={40}
              height={40}
            />
          ) : (
            <></>
          )}
          {row.type === "1904" ? (
            <Image
              className="items-center self-center"
              src={Img1904}
              layout="fixed"
              alt="Img1904"
              width={40}
              height={40}
            />
          ) : (
            <></>
          )}
        </div>

        <div className=" ml-[20px] self-center">
          <tr className="text-[32px] md:text-[36px] lg:text-[40px] font-semibold text-[#FF0000]">
            {row.total}
          </tr>
        </div>

        <div className=" ml-[20px] self-center">
          <tr className="text-[12px] md:text-[14px] lg:text-[14px] text-[#000000] font-bold">
            {row.name}
          </tr>
          <tr className="text-[12px] md:text-[14px] lg:text-[14px] text-[#000000]">
            
            {row.placa}
          </tr>
        </div>
      </div>
    );
  }
}
