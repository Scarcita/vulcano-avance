import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";


export default function ModalEditClients(props) {

  const {
    id, 

  } = props;
  const [showModal, setShowModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);


//   const actualizarDatos = async (formData) => {

//       setIsLoading(true)
//       var data = new FormData();

//       //console.log(formData.mediaUrl);
  
//       data.append("client_plan_id", client_plan_id);
//       data.append("social_network", formData.social_network);
//       data.append("type", formData.type);
//       data.append("planned_datetime", plannedDatetimeForm);
//       data.append("title", titleForm);
//       data.append("subtitle", subtitleForm);
//       data.append("instructions", instructionsForm);
//       data.append("post_copy", postCopyForm);
      
//       if(formData.mediaUrl.length !== 0){
//           data.append("media_url", formData.mediaUrl[0]);
//       }
  
//       fetch("http://slogan.com.bo/roadie/clientsPlansExtras/editMobile/" + id, {
//         method: 'POST',
//         body: data,
//       })
//         .then(response => response.json())
//         .then(data => {
//           console.log('VALOR ENDPOINTS EDIT: ', data);
//           setIsLoading(false)
//           if (data.status) { 
//               //setReloadPosts(!reloadPosts)
//               setShowModal(false)
//               console.log('edit endpoint: ' + data.status);
//           } else {
//               console.error(data.error)
//           }
//       })
  
//     }

  
  const validationSchema = Yup.object().shape({
  //title: Yup.string()
  //.required('title is required'),
 });

  const formOptions = { resolver: yupResolver(validationSchema) };

  // get functions to build form with useForm() hook
  const { register, handleSubmit, reset, formState } = useForm(formOptions);
  const { errors } = formState;

  function onSubmit(data) {
      alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
      console.log('string is NOT empty')
      actualizarDatos(data)
      return false;
      
  }

    return (
        <>
            <div className="flex items-center min-h-screen px-4 py-8">
                <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#F6F6FA] rounded-md shadow-lg">
                    <div>
                        <h4 className="text-lg font-medium text-gray-800 text-left">
                            EDIT
                        </h4>
                    </div>

                    <div>
                        <form onSubmit={handleSubmit(onSubmit)} >

                            <div className="mt-[20px] grid grid-cols-12 gap-4">
                              <div className='col-span-12 md:col-span-5 lg:col-span-5'>
                                <div className='text-[14px] font-semibold text-left'>
                                  Client
                                </div>
                                <div className='bg-[#FFFF] rounded-[8px] p-3 mt-[10px]'>
                                  <div className='flex flex row'>
                                    <div className="bg-[#E4E7EB] w-[48px] h-[48px] rounded-full justify-center">

                                    </div>
                                    <div className="ml-[10px]">
                                        <p className='text-[10px] font-normal text-left'>
                                            Register Into
                                        </p>
                                        <p className='text-[15px] font-semibold text-left'>
                                            CLIENTE NAME
                                        </p>
                                        <p className='text-[12px] font-normal text-left'>
                                            Created On: 
                                        </p>
                                    </div>
                                  </div>
                                  <div>
                                    <p className='text-[12px] text-left font-semibold'>
                                      Phone: <span className='text-[12px] font-normal text-left'>70738077</span>
                                    </p>
                                    <p className='text-[12px] text-left font-semibold'>
                                      Email: <span className='text-[12px] font-normal text-left'>wantdigitalagency@gmail.com</span>
                                    </p>
                                  </div>
                                </div>
                              </div>

                              <div className='col-span-12 md:col-span-7 lg:col-span-7'>
                                <div className='text-[14px] font-semibold text-left'>
                                  Activity details
                                </div>
                                <div className='bg-[#FFFF] rounded-[8px] p-3 mt-[10px]'>
                                 
                                </div>
                              </div>
                            </div>
                            <div className="flex flex-row justify-between mt-[20px]">

                                <div>
                                    <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                </div>

                                <div>
                                <button
                                    className="w-[72px] h-[30px] text-[#FFFFFF] bg-[#000] text-[12px] rounded-[9px] text-center items-center hover:bg-transparent hover:border-[#000] hover:border hover:text-[#000] justify-center mr-[10px]"
                                    onClick={() =>
                                        setShowModal(false)
                                    }
                                    disabled={isLoading}
                                >
                                    Cancel
                                </button>
                                <button
                                    className="w-[72px] h-[30px] text-[#FFFFFF] bg-[#3682F7] text-[12px] rounded-[9px] text-center items-center hover:bg-transparent hover:border-[#3682F7] hover:border hover:text-[#3682F7] justify-center"
                                    type="submit"
                                    disabled={isLoading}

                                >
                                    {isLoading ? <Dots className='m-auto' size={7} color={'#fff'}></Dots> : 'Update'}
                                    
                                </button>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}