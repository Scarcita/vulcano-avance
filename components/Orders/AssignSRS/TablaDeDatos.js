import React from 'react';
import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'
import { useEffect, useState } from 'react'

function TablaDeDatos() {
    const [showDots, setShowDots] = useState(true);
    const [data, setData] = useState([]);
    const [nPages, setNPages] = useState(25)
    const [currentPage, setCurrentPage] = useState(1);
    const N = 5;
    const pageNumbers = [...Array(N + 1).keys()].slice(1);
    console.log('almaa' + pageNumbers);



    useEffect(() => {
        const getData = () => {
            // fetch('https://slogan.com.bo/vulcano/orders/all/' + currentPage)
            fetch('https://slogan.com.bo/vulcano/orders/all/')
                .then(response => response.json())
                .then(data => {
                    console.log('hola' + data)
                    if (data.status) {
                        setData(data.data)
                        setCurrentPage(data.data.curent_page)
                        setNPages(data.data.total_pages)
                    } else {
                        console.error(data.error)
                    }
                })
                .then(setShowDots(false))
        }
        getData();
    }, [currentPage])

    return (
        // showDots ?
        //     <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
        //         <Spinner color="#3682F7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
        //     </div>
        //     :
        <>
            <div className='w-full grid grid-cols-12' >
                <Table
                    showDots={showDots}
                    setShowDots={setShowDots}
                    data={data}
                    nPages={nPages}
                    currentPage={currentPage}
                    setCurrentPage={setCurrentPage}
                />
            </div>
        </>
    )
}


const Table = (props) => {

    const { data, nPages, currentPage, setCurrentPage, showDots, setShowDots } = props
    console.log('marta' + nPages);
    const splitData = (obj) => {
        data.forEach(element => {
            let date = element.created.split("T");
            element.created = date[0];
        });
        data = obj;
    };

    splitData(data);

    return (
        <div className=' lg:bg-[#fff]  bg-[#F6F6FA] w-full min-w-100 max-w-full col-span-12 md:col-span-12 '>
            <table className="w-full hidden lg:table">
                <thead className='bg-[#3682F7] h-[43px] text-[#FFFFFF] w-full '>
                    <tr className='text-[14px] font-bold'>
                        <th className='px-2 text-left'>#OT</th>
                        <th className='px-2 text-left'>Clientes</th>
                        <th className='px-2 text-left'>Marca</th>
                        <th className='px-2 text-left'>Modelo</th>
                        <th className='px-2 text-left'>Color</th>
                        <th className='px-2 text-left'>Placa</th>
                        <th className='px-2 text-left'>Creación</th>
                        <th className='px-2 text-left'>Promesa Ent.</th>
                        <th className='px-2 text-left'>Estado</th>
                    </tr>
                </thead>

                <>
                    {
                        showDots ?
                            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                                <Spinner color="#3682F7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                            </div>
                            :
                            <tbody className=''>
                                {data.map(row =>

                                    <tr key={row.id} className='h-[43px] border-b-2 border-[#E4E7EB]' >
                                        {/* #OT */}
                                        <td className='pl-[15px]'> {row.km} </td>
                                        {/* CLIENTE */}
                                        <td> {row.contact_name} </td>
                                        {/*  MARCA */}
                                        <td>
                                            <div className='flex gap-2'>
                                                <div className='flex justify-center items-center'>
                                                    <img
                                                        className='w-[27px] h-[27px] '
                                                        src={row.car.cars_models_version.cars_model.catalogues_record.additional_info}
                                                    />
                                                </div>
                                                <div className='flex  justify-center items-center'>
                                                    <p>
                                                        {row.car.cars_models_version.cars_model.catalogues_record.name}
                                                    </p>
                                                </div>
                                            </div>
                                        </td>
                                        {/* MODELO */}
                                        <td>
                                            {row.car.cars_models_version.cars_model.name}
                                        </td>
                                        {/* COLOR */}
                                        <td> {row.car.color_id} </td>
                                        {/* PLACA */}
                                        <td> {row.car.plate} </td>
                                        {/* CREACION */}
                                        <td> {row.created} </td>
                                        {/* PROMESA DE NTREGA  */}
                                        <td> {row.created} </td>
                                        {/* ESTADO */}
                                        <td>
                                            <div className={
                                                row.status == "ABIERTO" ?
                                                    "text-[#3682F7] border-[#3682F7] text-center font-normal rounded-xl  border-[2px]"
                                                    : row.status == "EN PAUSA" ?
                                                        "text-[#D6312D] border-[#D6312D] text-center font-normal rounded-xl  border-[2px]"
                                                        : row.status == "EN CURSO" ?
                                                            "text-[#84CC16] border-[#84CC16] text-center font-normal rounded-xl  border-[2px]"
                                                            : row.status == "FINALIZADO" ?
                                                                "text-[#4ADE80] border-[#4ADE80] text-center font-normal rounded-xl  border-[2px]"
                                                                : row.status == "CERRADO" ?
                                                                    "text-[#F1CA7B] border-[#F1CA7B] text-center font-normal rounded-xl  border-[2px]"
                                                                    : row.status == "ENTREGADO" ?
                                                                        "text-[#5EE592] border-[#5EE592] text-center font-normal rounded-xl  border-[2px]"
                                                                        : null}>
                                                {row.status.charAt(0).toUpperCase() + row.status.slice(1).toLowerCase()}
                                            </div>
                                        </td>
                                    </tr>
                                )}
                            </tbody>
                    }
                </>
            </table>

            <div className=" h-screen ">
                <div className="grid grid-cols-1 sm:grid-cols-2 gap-4 md:block lg:hidden">
                    <>
                        {
                            showDots ?
                                <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                                    <Spinner color="#3682F7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                                </div>
                                :
                                <div className=" space-y-3 px-2">
                                    {data.map(row =>
                                        <div key={data} className="grid grid-cols-3 items-center text-sm bg-[#fff] rounded-lg shadow-md px-2 py-2">
                                            <div className='m-auto' >
                                                <div className='mb-2'  >
                                                    <div className='text-[18px] font-ligh text-[#3682F7]'>
                                                        #OT
                                                    </div>
                                                    <div className="text-black font-bold">
                                                        {row.km}
                                                    </div>
                                                </div>

                                                <div className='mb-2'  >
                                                    <div className='text-[18px] font-ligh  text-[#3682F7]'>
                                                        Clientes
                                                    </div>
                                                    <div className="text-black font-bold">
                                                        {row.contact_name == null ?
                                                            'Sin nombre'
                                                            : row.contact_name
                                                        }
                                                    </div>
                                                </div>

                                                <div >
                                                    <div className='text-[18px] font-ligh  text-[#3682F7]'>
                                                        Marca
                                                    </div>
                                                    <div className="flex gap-2 text-black font-bold">
                                                        <div className='flex justify-center items-center'>
                                                            <img
                                                                className='w-[20px] h-[20px] '
                                                                src={row.car.cars_models_version.cars_model.catalogues_record.additional_info}
                                                            />
                                                        </div>{row.car.cars_models_version.cars_model.catalogues_record.name}
                                                    </div>
                                                </div>
                                            </div>

                                            <div className='m-auto' >
                                                <div className='mb-2'>
                                                    <div className='text-[18px] font-ligh text-[#3682F7]'>
                                                        Modelo
                                                    </div>
                                                    <div className="text-black font-bold">
                                                        {row.car.cars_models_version.cars_model.name}
                                                    </div>
                                                </div>

                                                <div className='mb-2'>
                                                    <div className='text-[18px] font-ligh  text-[#3682F7]'>
                                                        Color
                                                    </div>
                                                    <div className="text-black font-bold">
                                                        {row.car.color_id}
                                                    </div>
                                                </div>

                                                <div>
                                                    <div className='text-[18px] font-ligh  text-[#3682F7]'>
                                                        Placa
                                                    </div>
                                                    <div className="text-black font-bold">
                                                        {row.car.plate}
                                                    </div>
                                                </div>
                                            </div>

                                            <div className='m-auto' >
                                                <div className='mb-2'>
                                                    <div className='text-[18px] font-ligh text-[#3682F7]'>
                                                        Creación
                                                    </div>
                                                    <div className="text-black font-bold">
                                                        {row.created}
                                                    </div>
                                                </div>

                                                <div className='mb-2'>
                                                    <div className='text-[18px] font-ligh  text-[#3682F7]'>
                                                        Promesa
                                                    </div>
                                                    <div className="text-black font-bold">
                                                        {row.created}
                                                    </div>
                                                </div>

                                                <div>
                                                    <div className='text-[18px] font-ligh  text-[#3682F7]'>
                                                        Estado
                                                    </div>
                                                    <div
                                                        className={
                                                            row.status == "ABIERTO" ?
                                                                "text-[#3682F7] border-[#3682F7] text-center font-normal rounded-xl  border-[2px]"
                                                                : row.status == "EN PAUSA" ?
                                                                    "text-[#D6312D] border-[#D6312D] text-center font-normal rounded-xl  border-[2px]"
                                                                    : row.status == "EN CURSO" ?
                                                                        "text-[#84CC16] border-[#84CC16] text-center font-normal rounded-xl  border-[2px]"
                                                                        : row.status == "FINALIZADO" ?
                                                                            "text-[#4ADE80] border-[#4ADE80] text-center font-normal rounded-xl  border-[2px]"
                                                                            : row.status == "CERRADO" ?
                                                                                "text-[#F1CA7B] border-[#F1CA7B] text-center font-normal rounded-xl  border-[2px]"
                                                                                : row.status == "ENTREGADO" ?
                                                                                    "text-[#5EE592] border-[#5EE592] text-center font-normal rounded-xl  border-[2px]"
                                                                                    : null}>
                                                        {row.status}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )}
                                </div>
                        }
                    </>
                </div>
            </div>
            {/* <Pagination
                nPages={nPages}
                currentPage={currentPage}
                setCurrentPage={setCurrentPage}
            /> */}
        </div>
    );
};

// const Pagination = ({ nPages, currentPage, setCurrentPage }) => {
//     console.log('numeros ' + nPages, currentPage);
//     const pageNumbers = [...(Array(nPages + 1).keys())].slice(1)
//     const nextPage = () => {
//         if (currentPage !== nPages) setCurrentPage(currentPage + 1)
//     }
//     const prevPage = () => {
//         if (currentPage !== 1) setCurrentPage(currentPage - 1)
//     }
//     return (
//         <nav>
//             <ul className='pagination justify-content-center'>
//                 <li className="page-item">
//                     <a className="page-link"
//                         onClick={prevPage}
//                         href='#'>
//                         Previous
//                     </a>
//                 </li>
//                 {pageNumbers.map(pgNumber => (
//                     <li key={pgNumber}
//                         className={`page-item ${currentPage == pgNumber ? 'active' : ''} `} >

//                         <a onClick={() => setCurrentPage(pgNumber)}
//                             className='page-link'
//                             href='#'>
//                             {pgNumber}
//                         </a>
//                     </li>
//                 ))}
//                 <li className="page-item">
//                     <a className="page-link"
//                         onClick={nextPage}
//                         href='#'>
//                         Next
//                     </a>
//                 </li>
//             </ul>
//         </nav>
//     )
// }

export default TablaDeDatos;