import Image from "next/image";

import imagenFord from '../../../assets/ford.png'

// import  imagenFord from '../../assets/ford.png'
function DatosDelAuto() {
    return (
     
            <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12">
                    <div className="flex justify-end text-end self-center">
                        <div className="mr-[10px] self-center">
                            <p className="text-14px font-semibold">
                                Juan Camacho A.
                            </p>
                            <p className=" text-[14px] text-[#ACB5BD]">
                            ASESOR DE SERVICIO
                            </p>
                        </div>
                        <div className="w-[62px] h-[62px] rounded-full bg-[#E4E7EB] ">

                        </div>
                    </div>
                </div>
                <div className='col-span-12 md:col-span-12 lg:col-span-12 h-full bg-[#3682F7] mt-[20px]'>
                    <div className='justify-center items-center text-center mt-[20px]'>
                        <Image
                            src={imagenFord}
                            layout='fixed'
                            alt='imagenFord'
                        />
                    </div>
                    <div className="ml-[30px] mr-[30px]">
                        <div className='pt-[10px]'>
                            <p className='text-[12px] font-normal text-[#FFFF]'>Marca</p>
                            <p className='text-[14px] font-normal text-[#FFFF]'>MERCEDES BENZ</p>
                        </div>
                        <div className='pt-[10px]'>
                            <p className='text-[12px] font-normal text-[#FFFF]'>Modelo</p>
                            <p className='text-[14px] font-normal text-[#FFFF]'>Modelo</p>
                        </div>
                        <div className='pt-[10px]'>
                            <p className='text-[12px] font-normal text-[#FFFF]'>Versión</p>
                            <p className='text-[14px] font-normal text-[#FFFF]'>Versión</p>
                        </div>
                        <div className='pt-[10px]'>
                            <p className='text-[12px] font-normal text-[#FFFF]'>Color</p>
                            <p className='text-[14px] font-normal text-[#FFFF]'>Color</p>
                        </div>
                        <div className='pt-[10px]'>
                            <p className='text-[12px] font-normal text-[#FFFF]'>Placa</p>
                            <p className='text-[14px] font-normal text-[#FFFF]'>Placa</p>
                        </div>
                        <div className='pt-[10px]'>
                            <p className='text-[12px] font-normal text-[#FFFF]'>VIN</p>
                            <p className='text-[14px] font-normal text-[#FFFF]'>1234567809123RGTF</p>
                        </div>
                        <div className='pt-[10px]'>
                            <p className='text-[12px] font-normal text-[#FFFF]'>Kilometraje</p>
                            <p className='text-[14px] font-normal text-[#FFFF]'>7779875</p>
                        </div>
                        <div className='pt-[10px]'>
                            <p className='text-[12px] font-normal text-[#FFFF]'>Transmisión</p>
                            <p className='text-[14px] font-normal text-[#FFFF]'>AUTOMÁTICA</p>
                        </div>
                    </div>

                <div className='flex flex-col justify-center items-center bg-[#3682F7] mt-[20px]' >
                    <button className=' w-[200px] h-8 border-white border-2 rounded-lg text-[13px] text-[#FFFF] m-3'>Editar Servicios Requeridos</button>
                    <button className=' w-[200px] h-8 border-white border-2 rounded-lg text-[#FFFF] text-[#FFFF] m-2'>Anular Orden de Trabajo</button>
                    <button className=' w-[200px] h-8 border-white border-2 rounded-lg text-[13px] text-[#FFFF] m-2'>Historial de Vehículo</button>
                    <button className=' w-[200px] h-8 bg-[#FFFF] border-2 rounded-lg text-[13px] text-[#3682F7] m-2'>Marcar como Entregado</button>
                    <button className=' w-[200px] h-8 bg-[#FFFF] border-2 rounded-lg text-[13px] text-[#3682F7] m-2'>Imprimir Pre-Factura</button>

                </div>

            </div>
                
            </div>
       
    )
}
export default DatosDelAuto;