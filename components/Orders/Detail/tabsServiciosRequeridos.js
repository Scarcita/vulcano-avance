import React from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import TablaServiciosRequeridos from "./TablaServiciosRequeridos";


export default function TabsServicesRequiare() {

    return(
            <div>
                <Tabs>
                    <TabList className="grid grid-cols-12 focus:outline-none  focus:text-[#3682F7] ">
                        <Tab className="col-span-6 md:col-span-4 lg:col-span-3 self-center text-[12px] focus:text-[#3682F7] rounded-tl-lg px-2 py-2">
                        {" "}
                        Servicios Requeridos
                        </Tab>
                        <Tab className="col-span-6 md:col-span-4 lg:col-span-3 self-center text-[12px] focus:text-[#3682F7] rounded-tl-lg px-2 py-2">
                        Observaciones de Cliente
                        </Tab>
                    </TabList>

                    <TabPanel>
                        <div className="grid grid-cols-12">
                            <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                                <TablaServiciosRequeridos/>
                            </div>
                        </div>
                    </TabPanel>
                    <TabPanel>
                        <h2>Any content 2</h2>
                    </TabPanel>
                </Tabs>
            </div>
    )
}