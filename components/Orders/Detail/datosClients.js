function DatosClients() {
    return (
       
            <div className='grid grid-cols-12 gap-3'>
                <div className="col-span-12 md:col-span-12 lg:col-span-5">
                    <p className='text-left text-[12px] text-[#ACB5BD]'>
                        Cliente Nombre:
                    </p>
                    <p className='text-left text-[11px] text-[#000]'>
                        GOBIERNO AUTÓNOMO DE COCHABAMBA GAMLC (SEUWS)
                    </p>
                </div>
                <div className="col-span-12 md:col-span-12 lg:col-span-3">
                    <p className='text-left text-[12px] text-[#ACB5BD]'>
                        Celular:
                    </p>
                    <p className='text-left text-[11px] text-[#000]'>
                        +5593328773237
                    </p>
                </div>
                <div className="col-span-12 md:col-span-12 lg:col-span-4">
                    <p className='text-left text-[12px] text-[#ACB5BD]'>
                        Email:
                    </p>
                    <p className='text-left text-[11px] text-[#000]'>
                        Casefs.eruhfse877823@gmail.com
                    </p>
                </div>
                <div className="col-span-12 md:col-span-12 lg:col-span-4">
                    <p className='text-left text-[12px] text-[#ACB5BD]'>
                        Facturación Nombre:
                    </p>
                    <p className='text-left text-[11px] text-[#000]'>
                        GOBIERNO AUTÓNOMO DE COCHABAMBA GAMLC (SEUWS)
                    </p>
                </div>
                <div className="col-span-12 md:col-span-12 lg:col-span-2">
                    <p className='text-left text-[12px] text-[#ACB5BD]'>
                        C.I./NIT:
                    </p>
                    <p className='text-left text-[11px] text-[#000]'>
                        87813246665
                    </p>
                </div>
                <div className="col-span-12 md:col-span-12 lg:col-span-4">
                    <p className='text-left text-[12px] text-[#ACB5BD]'>
                        Contacto Nombre:
                    </p>
                    <p className='text-left text-[11px] text-[#000]'>
                        GOBIERNO AUTÓNOMO DE COCHABAMBA GAMLC (SEUWS)
                    </p>
                </div>
                <div className="col-span-12 md:col-span-12 lg:col-span-2">
                    <p className='text-left text-[12px] text-[#ACB5BD]'>
                        Celular:
                    </p>
                    <p className='text-left text-[11px] text-[#000]'>
                        87813246665
                    </p>
                </div>
            </div>
    );
};
export default DatosClients;