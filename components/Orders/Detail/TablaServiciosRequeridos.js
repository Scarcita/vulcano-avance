import React from 'react';
import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'
import { useEffect, useState } from 'react'
import ModalEditServiceRequire from './modals/modalEditServicesRequiare.';
import ModalDeleteServicios from './modals/modalDeleteServiceRequiare';
import ModalDeleteServicesRequire from './modals/modalDeleteServiceRequiare';

const TablaServiciosRequeridos = () => {
    const [isLoading, setIsLoading] = useState(true);
    const [data, setData] = useState([]);
    const headlist = ["Nombre", "PrecioRef.", "Precio Hrs.", "Hrs. hombre", "Cliente", "Empresa", "Garantía", "Mecánico", "", ""];


    useEffect(() => {
        setIsLoading(true);
        fetch('https://slogan.com.bo/vulcano/orders/all/abierto')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log(data.data);
                    setData(data.data)

                } else {
                    console.error(data.error)
                }
                setIsLoading(false);
            }) 
    }, [])

    return (
        isLoading ?
            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                <Spinner color="#3682F7" size={14} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
            <>
                <div style={{maxHeight: '220px', overflowY: 'auto'}}>
                    <TablaProductos headlist={headlist} data={data} />

                </div>
            </>

    )
};

const TablaProductos = (props) => {
    const { data, headlist } = props;

    return (

        <div className='hidden lg:block md:block'>
            <table className="w-full h-[220px] bg-[#FFFFFF] rounded-t-[24px]">
                <thead className="bg-[#3682F7] h-[40px] ">
                    <tr>
                        {headlist.map((header) => (
                        <th
                            key={headlist.id}
                            className="text-[14px] font-medium text-[#fff]"
                        >
                            {header}
                        </th>
                        ))}
                    </tr>
                </thead>

                <tbody className="divide-y px-[10px]">
                    {data.map(row =>

                        <tr key={row.id}>

                            <td className='text-[12px] pl-[10px]'>
                                {row.client.name}
                            </td>

                            <td className='text-center text-[12px]'>
                                {row.total}
                            </td>

                            <td className='text-center '>
                                {row.subtotal}
                            </td>

                            <td className='text-center'>
                                {row.discount}
                            </td>

                            <td className='text-[12px]'>
                                {row.client.name}
                            </td>

                            <td className='text-[12px]'>
                                {row.client.name}
                            </td>

                            <td className='text-[12px]'>
                                {row.car.transmission}
                            </td>

                            <td className='text-[12px]'>
                                {row.contact_name}
                            </td>
                            <td>
                                <ModalEditServiceRequire/>
                            </td>
                            <td>
                                <ModalDeleteServicesRequire/>
                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
        </div>
    );
};

export default TablaServiciosRequeridos;