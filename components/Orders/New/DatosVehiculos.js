import React, { useEffect, useState, useRef } from "react";
import Image from "next/image";

import { Bounce, Sentry, Squares } from "react-activity";
import "react-activity/dist/library.css";

import imagenOpciones from "../../../public/opciones.svg";

import Select from "react-select";
import CreatableSelect from "react-select/creatable";

function DatosVehiculos(props) {
  const selectPlateRef = useRef();
  const selectVinRef = useRef();
  const selectBrandRef = useRef();
  const selectModelRef = useRef();
  const selectVersionRef = useRef();
  const selectColorRef = useRef();
  const selectTransmissionRef = useRef();

  const { setDatosVehiculo } = props;

  const [isLocked, setIsLocked] = useState(false);
  const [dataFilled, setDataFilled] = useState(false);
  const [reload, setReload] = useState(false);

  const [sendingData, setSendingData] = useState(false);
  const [doneMessage, setDoneMessage] = useState(false);

  const [carDataLoading, setCarDataLoading] = useState(false);
  const [newPlateLoading, setNewPlateLoading] = useState(false);
  const [newVinLoading, setNewVinLoading] = useState(false);
  const [plateLoading, setPlateLoading] = useState(false);
  const [vinLoading, setVinLoading] = useState(false);
  const [brandLoading, setBrandLoading] = useState(false);
  const [modelLoading, setModelLoading] = useState(false);
  const [versionLoading, setVersionLoading] = useState(false);
  const [colorLoading, setColorLoading] = useState(false);
  const [transmisionLoading, setTransmisionLoading] = useState(false);

  const [selectedCarId, setSelectedCarId] = useState(0);
  const [selectedPlateId, setSelectedPlateId] = useState(null);
  const [selectedVinId, setSelectedVinId] = useState(null);
  const [selectedBrandId, setSelectedBrandId] = useState(null);
  const [selectedModelId, setSelectedModelId] = useState(null);
  const [selectedVersionId, setSelectedVersionId] = useState(null);
  const [selectedColorId, setSelectedColorId] = useState(null);
  const [selectedTransmission, setSelectedTransmission] = useState(null);

  const [placa, setPlaca] = useState("");
  const [vin, setVin] = useState("");
  const [marca, setMarca] = useState("");
  const [modelo, setModelo] = useState("");
  const [version, setVersion] = useState("");
  const [color, setColor] = useState("");
  const [anio, setAnio] = useState("");
  const [transmision, setTransmision] = useState("");
  const [kilometraje, setKilometraje] = useState("");

  const [plateOptions, setPlateOptions] = useState([]);
  const [vinOptions, setVinOptions] = useState([]);
  const [brandOptions, setBrandOptions] = useState([]);
  const [modelOptions, setModelOptions] = useState([]);
  const [versionOptions, setVersionOptions] = useState([]);
  const [colorOptions, setColorOptions] = useState([]);
  const [transmissionOptions, setTransmissionOptions] = useState([]);

  useEffect(() => {
    setDatosVehiculo({
      id: selectedCarId,
      placa: placa,
      vin: vin,
      marca: marca,
      modelo: modelo,
      version: version,
      color: color,
      anio: anio,
      transmision: transmision,
    });
  }, [
    selectedCarId,
    placa,
    vin,
    marca,
    modelo,
    version,
    color,
    anio,
    transmision,
    setDatosVehiculo,
  ]);

  useEffect(() => {
    getDatosPlaca();
    getDatosVin();
    getBrands();
    getColors();
    getTransmision();
  }, [reload]);

  useEffect(() => {
    getDatosPlaca();
    getDatosVin();
    getBrands();
    getColors();
    getTransmision();
  }, []);

  const getDatosPlaca = () => {
    setPlateLoading(true);
    fetch("https://slogan.com.bo/vulcano/cars/allBy/plate")
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          const dataKeys = Object.keys(data.data);

          let indexedData = [];

          for (let i = 0; i < dataKeys.length; i++) {
            let index = dataKeys[i];
            if (data.data[index] !== null) {
              indexedData.push({
                value: index,
                label: data.data[index],
              });
            }
          }

          setPlateOptions(indexedData);
        } else {
          console.error("getDatosPlaca: " + data.error);
        }
        setPlateLoading(false);
      });
  };
  const getDatosVin = () => {
    setVinLoading(true);
    fetch("https://slogan.com.bo/vulcano/cars/allBy/vin")
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          const dataKeys = Object.keys(data.data);

          var indexedData = [];

          for (let i = 0; i < dataKeys.length; i++) {
            let index = dataKeys[i];
            if (data.data[index] !== null) {
              // indexedData.push({ id: index, name: data.data[index] });
              indexedData.push({
                value: index,
                label: data.data[index],
              });
            }
          }

          setVinOptions(indexedData);
        } else {
          console.error("getDatosVin: " + data.error);
        }
        setCarDataLoading(false);
        setVinLoading(false);
      });
  };

  const getBrands = () => {
    setBrandLoading(true);
    fetch("https://slogan.com.bo/vulcano/catalogues/all/brands")
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          var indexedData = [];
          for (let i = 0; i < data.data.length; i++) {
            indexedData.push({
              value: data.data[i].id,
              label: data.data[i].name,
            });
          }
          setBrandOptions(indexedData);
        } else {
          console.error("getBrands: " + data.error);
        }
        setBrandLoading(false);
      });
  };

  const getColors = () => {
    setColorLoading(true);
    fetch("https://slogan.com.bo/vulcano/catalogues/all/colors")
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          var indexedData = [];
          for (let i = 0; i < data.data.length; i++) {
            indexedData.push({
              value: data.data[i].id,
              label: data.data[i].name,
            });
          }
          setColorOptions(indexedData);
        } else {
          console.error("getColors: " + data.error);
        }
        setColorLoading(false);
      });
  };

  const getTransmision = () => {
    setTransmisionLoading(true);
    setTransmissionOptions([
      { value: "MANUAL", label: "MANUAL" },
      { value: "AUTOMATICA", label: "AUTOMATICA" },
    ]);
    setTransmisionLoading(false);
  };

  useEffect(() => {
    if (selectedBrandId !== 0 && selectedBrandId !== null) {
      getModels(selectedBrandId);
    }
  }, [selectedBrandId, dataFilled]);

  const getModels = (id) => {
    setModelLoading(true);
    fetch("https://slogan.com.bo/vulcano/carsModels/getByBrand/" + id)
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          var indexedData = [];
          for (let i = 0; i < data.data.length; i++) {
            indexedData.push({
              value: data.data[i].id,
              label: data.data[i].name,
            });
          }
          console.log("Models: " + JSON.stringify(indexedData));
          setModelOptions(indexedData);
        } else {
          console.error("getModels: " + data.error);
        }
        setModelLoading(false);
      });
  };

  useEffect(() => {
    if (selectedModelId !== null && selectedModelId !== 0) {
      getVersions(selectedModelId);
    }
  }, [selectedModelId, dataFilled]);

  const getVersions = (id) => {
    setVersionLoading(true);
    console.log("Model ID: " + id);
    fetch("https://slogan.com.bo/vulcano/carsModelsVersions/getByModel/" + id)
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          var indexedData = [];
          for (let i = 0; i < data.data.length; i++) {
            indexedData.push({
              value: data.data[i].id,
              label: data.data[i].name,
            });
          }
          setVersionOptions(indexedData);
        } else {
          console.error("getVersions: " + data.error);
        }
        setVersionLoading(false);
      });
  };

  const handleOnSelect = (id) => {
    setIsLocked(false);
    setCarDataLoading(true);
    fetch("https://slogan.com.bo/vulcano/cars/getById/" + id)
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          setPlaca(data.data.plate);
          setVin(data.data.vin);
          setMarca(data.data.brand_id);
          setModelo(data.data.model_id);
          setVersion(data.data.version_id);
          setAnio(data.data.year);
          setKilometraje(data.data.km);
          setTransmision(data.data.transmission);
          setColor(data.data.color_id);

          setSelectedCarId(data.data.id);

          let tempPlateOptions = [
            {
              value: data.data.id,
              label: data.data.plate,
            },
          ];
          for (let i = 0; i < plateOptions.length; i++) {
            if (plateOptions[i].label !== data.data.plate) {
              tempPlateOptions.push(plateOptions[i]);
            }
          }
          setPlateOptions(tempPlateOptions);
          setSelectedPlateId(0);

          let tempVinOptions = [
            {
              value: data.data.id,
              label: data.data.vin,
            },
          ];
          for (let i = 0; i < vinOptions.length; i++) {
            if (vinOptions[i].label !== data.data.vin) {
              tempVinOptions.push(vinOptions[i]);
            }
          }
          setVinOptions(tempVinOptions);
          setSelectedVinId(0);

          let tempBrandOptions = [
            {
              value: data.data.brand_id,
              label: data.data.brand,
            },
          ];
          for (let i = 0; i < brandOptions.length; i++) {
            if (brandOptions[i].label !== data.data.brand) {
              tempBrandOptions.push(brandOptions[i]);
            }
          }
          setBrandOptions(tempBrandOptions);
          setSelectedBrandId(0);

          setModelOptions([
            {
              value: data.data.model_id,
              label: data.data.model,
            },
          ]);
          setSelectedModelId(0);
          setVersionOptions([
            {
              value: data.data.version_id,
              label: data.data.version,
            },
          ]);
          setSelectedVersionId(0);

          let tempColorOptions = [
            {
              value: data.data.color_id,
              label: data.data.color,
            },
          ];
          for (let i = 0; i < colorOptions.length; i++) {
            if (colorOptions[i].label !== data.data.color) {
              tempColorOptions.push(colorOptions[i]);
            }
          }
          setColorOptions(tempColorOptions);
          setSelectedColorId(0);

          let tempTransmissionOptions = [
            {
              value: data.data.transmission,
              label: data.data.transmission,
            },
          ];
          for (let i = 0; i < transmissionOptions.length; i++) {
            if (transmissionOptions[i].label !== data.data.transmission) {
              tempTransmissionOptions.push(transmissionOptions[i]);
            }
          }
          setTransmissionOptions(tempTransmissionOptions);
          setSelectedTransmission(0);

          setCarDataLoading(false);
          setDataFilled(true);
          setIsLocked(true);
        } else {
          console.error("handleOnSelect: " + data.error);
        }
      });
  };

  const handleOnCreatePlate = (inputValue) => {
    setNewPlateLoading(true);
    setSelectedCarId(0);
    let tempPlateOptions = [
      {
        value: 0,
        label: inputValue.toUpperCase(),
      },
    ];
    for (let i = 0; i < plateOptions.length; i++) {
      if (plateOptions[i].label !== inputValue) {
        tempPlateOptions.push(plateOptions[i]);
      }
    }
    setPlateOptions(tempPlateOptions);
    setSelectedPlateId(0);

    setPlaca(inputValue);
    setTimeout(() => {
      setNewPlateLoading(false);
    }, 500);
  };

  const handleOnCreateVin = (inputValue) => {
    setNewVinLoading(true);
    setSelectedCarId(0);
    let tempVinOptions = [
      {
        value: 0,
        label: inputValue,
      },
    ];
    for (let i = 0; i < vinOptions.length; i++) {
      if (vinOptions[i].label !== inputValue) {
        tempVinOptions.push(vinOptions[i]);
      }
    }
    setVinOptions(tempVinOptions);
    setSelectedVinId(0);

    setVin(inputValue);

    setTimeout(() => {
      setNewVinLoading(false);
    }, 500);
  };

  const ClearIndicator = (props) => {
    const {
      getStyles,
      innerProps: { ref, ...restInnerProps },
    } = props;
    return (
      <div
        {...restInnerProps}
        style={getStyles("clearIndicator", props)}
        onClick={() => {
          setPlaca("");
        }}
      >
        <div
          style={{
            padding: "0px",
            margin: "0px",
            cursor: "pointer",
          }}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="w-5 h-5"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M6 18L18 6M6 6l12 12"
            />
          </svg>
        </div>
      </div>
    );
  };

  const formatResult = (item) => {
    return (
      <div className="flex flex-row">
        {/* <Image
          src={item.additional_info}
          alt="brand"
          width={30}
          height={30}
          className="rounded-full"
        /> */}
        <p className="ml-2">{item.name}</p>
      </div>
    );
  };

  const addNewCar = () => {
    setSendingData(true);
    const data = new FormData();
    data.append("plate", placa.toUpperCase());
    data.append("vin", vin);
    data.append("version_id", selectedVersionId);
    data.append("color_id", selectedColorId);
    data.append("year", anio);
    data.append("transmission", selectedTransmission);

    fetch("https://slogan.com.bo/vulcano/cars/addMobile", {
      method: "POST",
      body: data,
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          console.log("Automovil creado con exito");
          console.log("New car: " + Object.entries(data.data));

          setReload(!reload);
          handleOnSelect(data.data.id);

          setSendingData(false);
          setDoneMessage(true);

          setTimeout(() => {
            setDoneMessage(false);
          }, 2000);
        } else {
          console.error("addNewCar: " + data.error);
        }
      });
  };

  const editCarData = () => {
    setSendingData(true);
    const data = new FormData();
    data.append("plate", placa.toUpperCase());
    data.append("vin", vin);
    data.append("version_id", version);
    data.append("color_id", color);
    data.append("year", anio);
    data.append("transmission", transmision);

    fetch("https://slogan.com.bo/vulcano/cars/editMobile/" + selectedCarId, {
      method: "POST",
      body: data,
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("editCarData Data: " + data.status);
        if (data.status) {
          console.log("Automovil editado con exito");
          console.log("Updated car: " + Object.entries(data.data));

          handleOnSelect(selectedCarId);

          setSendingData(false);
          setDoneMessage(true);

          setTimeout(() => {
            setDoneMessage(false);
          }, 2000);
        } else {
          console.error("editCarData: " + data.error);
        }
      });
  };

  return (
    // COTENERDOR PRINCIPAL
    <form
      className="
      w-full h-full flex flex-col justify-around items-center
      "
    >
      {/* CABECERA */}
      <div
        className="
      w-full h-14 flex flex-row justify-start items-center
      "
      >
        <p
          className="
        text-lg font-bold
        "
        >
          Vehículo
        </p>
      </div>

      {/* CUERPO */}
      {sendingData ? (
        <div
          className="
          w-full h-full flex flex-col justify-center items-center px-4 bg-[#fff] rounded-lg shadow-lg rounded-lg
          "
        >
          <Sentry size={100} color="#3b82f6" />
        </div>
      ) : doneMessage ? (
        <div
          className="
          w-full h-full flex flex-col justify-center items-center px-4 bg-[#fff] rounded-lg shadow-lg rounded-lg p-4
          "
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth="1.5"
            stroke="#3b82f6"
            className="w-24 h-24"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M4.5 12.75l6 6 9-13.5"
            />
          </svg>
          <p
            className="
              font-bold text-3xl mt-4 mb-4 text-[#3b82f6]
              "
          >
            ¡Listo!
          </p>
        </div>
      ) : (
        <div
          className="
      w-full h-full flex flex-col justify-center items-center px-4 bg-[#fff] rounded-lg shadow-lg rounded-lg
      "
        >
          {/* PLACA Y VIN */}
          <div
            className="
        w-full h-fit flex flex-col justify-between items-center py-2
        {/*DESKTOP*/}
        lg:flex-row
        "
          >
            {/* PLACA */}
            <div
              className="
          w-full 
          {/*DESKTOP*/}
          lg:w-3/6
          "
            >
              <div
                className="
            w-full flex flex-row justify-start items-center
            "
              >
                <label
                  className="
              font-light text-[#A5A5A5] text-lg
              "
                >
                  Placa
                </label>
              </div>
              <div
                className="
            w-full h-12 flex flex-row justify-center items-center bg-[#000] rounded-lg
            "
              >
                {carDataLoading == true || newPlateLoading == true ? (
                  <Bounce
                    size={20}
                    color="#D3D3D3"
                    speed={1}
                    animating={true}
                    style={{
                      marginBottom: "0.1rem",
                    }}
                  />
                ) : (
                  <CreatableSelect
                    className="w-full h-full"
                    ref={selectPlateRef}
                    defaultValue={plateOptions[selectedPlateId]}
                    options={plateOptions}
                    onChange={(e) => {
                      e == null
                        ? setSelectedCarId("")
                        : (setSelectedCarId(e.value), handleOnSelect(e.value));
                    }}
                    onCreateOption={(e) => {
                      handleOnCreatePlate(e);
                    }}
                    isLoading={plateLoading}
                    isDisabled={isLocked || plateLoading}
                    isClearable={true}
                    required
                    placeholder="Buscar placa"
                    noOptionsMessage={() => "No hay resultados"}
                    components={{
                      DropdownIndicator: () => null,
                      IndicatorSeparator: () => null,
                    }}
                    styles={{
                      control: (provided, state) => ({
                        ...provided,
                        height: "100%",
                        border: "none",
                        borderRadius: "10px",
                        backgroundColor: "#000",
                        boxShadow: "none",
                        "&:hover": {
                          border: "none",
                          boxShadow: "none",
                        },
                      }),
                      option: (provided, state) => ({
                        ...provided,
                        color: "#000",
                        backgroundColor: state.isSelected
                          ? "#3b82f6"
                          : state.isFocused
                          ? "#e2e8f0"
                          : "#fff",
                        "&:hover": {
                          backgroundColor: "#e2e8f0",
                        },
                      }),
                      menu: (provided, state) => ({
                        ...provided,
                        zIndex: "9999",
                      }),
                      singleValue: (provided, state) => ({
                        ...provided,
                        color: "#fff",
                      }),
                      placeholder: (provided, state) => ({
                        ...provided,
                        color: "#fff",
                      }),
                      input: (provided, state) => ({
                        ...provided,
                        color: "#fff",
                      }),
                    }}
                  />
                )}
              </div>
            </div>

            {/* WHITESPACE */}
            <div
              className="
          w-16
          "
            ></div>

            {/* VIN */}
            <div
              className="
          w-full 
          {/*DESKTOP*/}
          lg:w-3/6
          "
            >
              <div
                className="
            w-full flex flex-row justify-start items-center
            "
              >
                <label
                  className="
              font-light text-[#A5A5A5] text-lg
              "
                >
                  VIN
                </label>
              </div>

              <div
                className="
                w-full h-12 flex flex-row justify-center items-center bg-[#D9D9D9] rounded-lg
            "
              >
                {carDataLoading == true || newVinLoading == true ? (
                  <Bounce
                    size={20}
                    color="#000"
                    speed={1}
                    animating={true}
                    style={{
                      marginBottom: "0.1rem",
                    }}
                  />
                ) : (
                  <CreatableSelect
                    className="w-full h-full"
                    ref={selectVinRef}
                    defaultValue={vinOptions[selectedVinId]}
                    options={vinOptions}
                    onChange={(e) => {
                      e == null
                        ? setSelectedCarId("")
                        : (setSelectedCarId(e.value), handleOnSelect(e.value));
                    }}
                    onCreateOption={(e) => {
                      handleOnCreateVin(e);
                    }}
                    isLoading={vinLoading}
                    isDisabled={isLocked || vinLoading}
                    isClearable={true}
                    required
                    placeholder="Buscar VIN"
                    noOptionsMessage={() => "No hay resultados"}
                    components={{
                      DropdownIndicator: () => null,
                      IndicatorSeparator: () => null,
                    }}
                    styles={{
                      control: (provided, state) => ({
                        ...provided,
                        height: "100%",
                        border: "none",
                        borderRadius: "10px",
                        backgroundColor: "#D9D9D9",
                        boxShadow: "none",
                        "&:hover": {
                          border: "none",
                          boxShadow: "none",
                        },
                      }),
                      option: (provided, state) => ({
                        ...provided,
                        color: "#000",
                        backgroundColor: state.isSelected
                          ? "#3b82f6"
                          : state.isFocused
                          ? "#e2e8f0"
                          : "#fff",
                        "&:hover": {
                          backgroundColor: "#e2e8f0",
                        },
                      }),
                      menu: (provided, state) => ({
                        ...provided,
                        zIndex: "9999",
                      }),
                      singleValue: (provided, state) => ({
                        ...provided,
                        color: "#000",
                      }),
                      placeholder: (provided, state) => ({
                        ...provided,
                        color: "#000",
                      }),
                      input: (provided, state) => ({
                        ...provided,
                        color: "#000",
                      }),
                    }}
                  />
                )}
              </div>
            </div>
          </div>

          {/* DATOS */}
          <div
            className="
        w-full h-full flex flex-col justify-start items-center
        {/*DESKTOP*/}
        lg:justify-evenly
        "
          >
            {/* TITULO E ICONOS */}
            <div
              className="
        w-full h-14 flex flex-row justify-between items-center
        {/*DESKTOP*/}
        lg:h-8
        "
            >
              {/* TITULO */}
              <div className="w-full flex flex-row justify-start items-center">
                <p className="font-medium text-lg"> Detalles de registro</p>
              </div>
              {/* ICONOS */}
              <div className="flex flex-row justify-center items-center">
                <div
                  className="w-[19px] h-[18px] m-2"
                  onClick={() => {
                    setIsLocked(!isLocked);
                  }}
                >
                  {isLocked == false ? (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      fill="currentColor"
                      className="
                    text-[#D3D3D3] w-[19px] h-[18px] cursor-pointer
                    {/*HOVER*/}
                    hover:text-[#000]
                    "
                    >
                      <path d="M18 1.5c2.9 0 5.25 2.35 5.25 5.25v3.75a.75.75 0 01-1.5 0V6.75a3.75 3.75 0 10-7.5 0v3a3 3 0 013 3v6.75a3 3 0 01-3 3H3.75a3 3 0 01-3-3v-6.75a3 3 0 013-3h9v-3c0-2.9 2.35-5.25 5.25-5.25z" />
                    </svg>
                  ) : (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      fill="currentColor"
                      className="
                    text-[#D3D3D3] w-[19px] h-[18px] cursor-pointer
                    {/*HOVER*/}
                    hover:text-[#000] 
                    "
                    >
                      <path
                        fillRule="evenodd"
                        d="M12 1.5a5.25 5.25 0 00-5.25 5.25v3a3 3 0 00-3 3v6.75a3 3 0 003 3h10.5a3 3 0 003-3v-6.75a3 3 0 00-3-3v-3c0-2.9-2.35-5.25-5.25-5.25zm3.75 8.25v-3a3.75 3.75 0 10-7.5 0v3h7.5z"
                        clipRule="evenodd"
                      />
                    </svg>
                  )}
                </div>
                <div className="w-[5px] h-[19px] m-2">
                  <Image
                    src={imagenOpciones}
                    layout="responsive"
                    alt="imagenOpciones"
                  />
                </div>
              </div>
            </div>

            {/* MARCA Y MODELO */}
            <div
              className="
          w-full h-fit flex flex-col justify-around items-center
          {/*DESKTOP*/}
          lg:flex-row
          {/*TABLET*/}
          md:flex-row
          "
            >
              <div
                className="
            w-full flex flex-col justify-center items-center
            {/*DESKTOP*/}
            md:mr-4
            {/*TABLET*/}
            md:w-1/2 
            "
              >
                <label
                  className="
                w-full self-start text-s m text-[#A5A5A5] font-light
                "
                >
                  Marca
                </label>

                <div
                  className={`
              w-full h-10 flex flex-col justify-center items-center border-2 rounded-lg
              ${
                isLocked
                  ? "cursor-not-allowed"
                  : "border-[#D3D3D3] cursor-pointer"
              }
              `}
                >
                  {carDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <Select
                      className="w-full h-full"
                      ref={selectBrandRef}
                      defaultValue={brandOptions[selectedBrandId]}
                      onChange={(e) => {
                        e == null
                          ? setSelectedBrandId(null)
                          : setSelectedBrandId(e.value);

                        setSelectedModelId(null);
                        setSelectedVersionId(null);
                      }}
                      isLoading={brandLoading}
                      options={brandOptions}
                      isClearable={true}
                      required={true}
                      isDisabled={isLocked}
                      placeholder="Seleccione una marca"
                      noOptionsMessage={() => "No hay opciones"}
                      components={{
                        DropdownIndicator: () => null,
                        IndicatorSeparator: () => null,
                        ClearIndicator,
                      }}
                      styles={{
                        control: (base) => ({
                          ...base,
                          border: "none",
                          boxShadow: "none",
                          backgroundColor: "transparent",
                          height: "100%",
                          width: "100%",
                        }),
                        option: (base) => ({
                          ...base,
                          color: "#000",
                          backgroundColor: "##FFFFFF",
                          ":hover": {
                            backgroundColor: "#B6B6B6",
                          },
                        }),
                        singleValue: (base) => ({
                          ...base,
                          color: "#000",
                        }),
                        placeholder: (base) => ({
                          ...base,
                          color: "#A5A5A5",
                        }),
                      }}
                    />
                  )}
                </div>
              </div>

              <div
                className="
              w-full flex flex-col justify-center items-center
              {/*DESKTOP*/}
              lg:ml-4
              {/*TABLET*/}
              md:w-1/2 
            "
              >
                <label
                  className="
              w-full self-start text-s m text-[#A5A5A5] font-light
              "
                >
                  Modelo
                </label>

                <div
                  className={`
              w-full h-10 flex flex-col justify-center items-center border-2 rounded-lg
              ${
                isLocked
                  ? "cursor-not-allowed"
                  : "border-[#D3D3D3] cursor-pointer"
              }
              `}
                >
                  {carDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <Select
                      className="w-full h-full"
                      ref={selectModelRef}
                      defaultValue={modelOptions[selectedModelId]}
                      onChange={(e) => {
                        e == null
                          ? setSelectedModelId(null)
                          : setSelectedModelId(e.value);
                      }}
                      isLoading={modelLoading}
                      options={modelOptions}
                      isClearable={true}
                      required={true}
                      isDisabled={isLocked}
                      placeholder="Seleccione un modelo"
                      noOptionsMessage={() => "No hay opciones"}
                      components={{
                        DropdownIndicator: () => null,
                        IndicatorSeparator: () => null,
                        ClearIndicator,
                      }}
                      styles={{
                        control: (base) => ({
                          ...base,
                          border: "none",
                          boxShadow: "none",
                          backgroundColor: "transparent",
                          height: "100%",
                          width: "100%",
                        }),
                        option: (base) => ({
                          ...base,
                          color: "#000",
                          backgroundColor: "##FFFFFF",
                          ":hover": {
                            backgroundColor: "#B6B6B6",
                          },
                        }),
                        singleValue: (base) => ({
                          ...base,
                          color: "#000",
                        }),
                        placeholder: (base) => ({
                          ...base,
                          color: "#A5A5A5",
                        }),
                      }}
                    />
                  )}
                </div>
              </div>
            </div>

            {/* DEMAS DATOS */}
            <div
              className="
          w-full h-fit flex flex-col justify-center items-center          
          {/*TABLET*/}
          md:flex-row
          "
            >
              <div
                className="
            w-full h-full flex flex-col justify-center items-center
            {/*DESKTOP*/}
            lg:mr-2
            {/*TABLET*/}
            md:w-1/4
            "
              >
                <label
                  className="
              w-full self-start text-s m text-[#A5A5A5] font-light
              "
                >
                  Version
                </label>
                <div
                  className={`
              w-full h-10 flex flex-col justify-center items-center border-2 rounded-lg
              ${
                isLocked
                  ? "cursor-not-allowed"
                  : "border-[#D3D3D3] cursor-pointer"
              }
              `}
                >
                  {carDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <Select
                      className="w-full h-full"
                      ref={selectVersionRef}
                      defaultValue={versionOptions[selectedVersionId]}
                      onChange={(e) => {
                        e == null
                          ? setSelectedVersionId(null)
                          : setSelectedVersionId(e.value);
                      }}
                      isLoading={versionLoading}
                      options={versionOptions}
                      isClearable={true}
                      isDisabled={isLocked}
                      required={true}
                      placeholder="Elija una version"
                      noOptionsMessage={() => "No hay opciones"}
                      components={{
                        DropdownIndicator: () => null,
                        IndicatorSeparator: () => null,
                        ClearIndicator,
                      }}
                      styles={{
                        control: (base) => ({
                          ...base,
                          border: "none",
                          boxShadow: "none",
                          backgroundColor: "transparent",
                          height: "100%",
                          width: "100%",
                          overflow: "hidden",
                        }),
                        option: (base) => ({
                          ...base,
                          color: "#000",
                          backgroundColor: "##FFFFFF",
                          ":hover": {
                            backgroundColor: "#B6B6B6",
                          },
                        }),
                        singleValue: (base) => ({
                          ...base,
                          color: "#000",
                        }),
                        placeholder: (base) => ({
                          ...base,
                          color: "#A5A5A5",
                        }),
                      }}
                    />
                  )}
                </div>
              </div>

              <div
                className="
            w-full h-full flex flex-col justify-center items-center
            {/*DESKTOP*/}
            lg:mr-2
            {/*TABLET*/}
            md:w-1/4
            "
              >
                <label
                  className="
              w-full self-start text-s m text-[#A5A5A5] font-light
              "
                >
                  Color
                </label>
                <div
                  className={`
              w-full h-10 flex flex-col justify-center items-center border-2 rounded-lg
              ${
                isLocked
                  ? "cursor-not-allowed"
                  : "border-[#D3D3D3] cursor-pointer"
              }
              `}
                >
                  {carDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <Select
                      className="w-full h-full"
                      ref={selectColorRef}
                      defaultValue={colorOptions[selectedColorId]}
                      onChange={(e) => {
                        e == null
                          ? setSelectedColorId(null)
                          : setSelectedColorId(e.value);
                      }}
                      isLoading={colorLoading}
                      options={colorOptions}
                      isClearable={true}
                      isDisabled={isLocked}
                      required={true}
                      placeholder="Elija un color"
                      noOptionsMessage={() => "No hay opciones"}
                      components={{
                        DropdownIndicator: () => null,
                        IndicatorSeparator: () => null,
                        ClearIndicator,
                      }}
                      styles={{
                        control: (base) => ({
                          ...base,
                          border: "none",
                          boxShadow: "none",
                          backgroundColor: "transparent",
                          height: "100%",
                          width: "100%",
                        }),
                        option: (base) => ({
                          ...base,
                          color: "#000",
                          backgroundColor: "##FFFFFF",
                          ":hover": {
                            backgroundColor: "#B6B6B6",
                          },
                        }),
                        singleValue: (base) => ({
                          ...base,
                          color: "#000",
                        }),
                        placeholder: (base) => ({
                          ...base,
                          color: "#A5A5A5",
                        }),
                      }}
                    />
                  )}
                </div>
              </div>

              <div
                className="
            w-full h-full flex flex-col justify-center items-center
            {/*DESKTOP*/}
            lg:mr-2
            {/*TABLET*/}
            md:w-1/4
            "
              >
                <label
                  className="
              w-full self-start text-s m text-[#A5A5A5] font-light
              "
                >
                  Año
                </label>
                <div
                  className={`
              w-full h-10 flex flex-col justify-center items-center border-2 rounded-lg
              ${
                isLocked
                  ? "cursor-not-allowed"
                  : "border-[#D3D3D3] cursor-pointer"
              }
              `}
                >
                  {carDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <input
                      className="
                    w-full h-10 text-base text-left font-normal rounded-full px-2 outline-none mt-1
                    "
                      type={"number"}
                      disabled={isLocked}
                      placeholder="Seleccione un año"
                      value={anio}
                      onChange={(e) => {
                        if (e.target.value.length > 4) {
                          return;
                        }
                        setAnio(e.target.value);
                      }}
                    />
                  )}
                </div>
              </div>

              <div
                className="
            w-full h-full flex flex-col justify-center items-center
            {/*TABLET*/}
            md:w-1/4
            "
              >
                <label
                  className="
              w-full self-start text-s m text-[#A5A5A5] font-light
              "
                >
                  Transmision
                </label>
                <div
                  className={`
              w-full h-10 flex flex-col justify-center items-center border-2 rounded-lg
              ${
                isLocked
                  ? "cursor-not-allowed"
                  : "border-[#D3D3D3] cursor-pointer"
              }
              `}
                >
                  {carDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <Select
                      className="w-full h-full"
                      ref={selectTransmissionRef}
                      defaultValue={transmissionOptions[selectedTransmission]}
                      onChange={(e) => {
                        e == null
                          ? setSelectedTransmission(null)
                          : setSelectedTransmission(e.value);
                      }}
                      isLoading={transmisionLoading}
                      options={transmissionOptions}
                      isClearable={true}
                      isDisabled={isLocked}
                      required={true}
                      placeholder="Elija una transmision"
                      noOptionsMessage={() => "No hay opciones"}
                      components={{
                        DropdownIndicator: () => null,
                        IndicatorSeparator: () => null,
                        ClearIndicator,
                      }}
                      styles={{
                        control: (base) => ({
                          ...base,
                          border: "none",
                          boxShadow: "none",
                          backgroundColor: "transparent",
                          height: "100%",
                          width: "100%",
                        }),
                        option: (base) => ({
                          ...base,
                          color: "#000",
                          backgroundColor: "##FFFFFF",
                          ":hover": {
                            backgroundColor: "#B6B6B6",
                          },
                        }),
                        singleValue: (base) => ({
                          ...base,
                          color: "#000",
                        }),
                        placeholder: (base) => ({
                          ...base,
                          color: "#A5A5A5",
                        }),
                      }}
                    />
                  )}
                </div>
              </div>
            </div>
          </div>

          {/* BOTONES */}
          <div
            className="
          w-full h-14 flex flex-row justify-center items-center mb-2
          "
          >
            <div
              className="
            w-1/2 h-full flex flex-row justify-start items-center
            "
            >
              {selectedCarId !== 0 ? (
                <p
                  className="
              text-[#A5A5A5] text-[14px] font-Dosis
              "
                >
                  Kilometraje
                  {kilometraje === null
                    ? " aun no registrado"
                    : ": " + kilometraje + " km"}
                </p>
              ) : null}
            </div>

            <div
              className="
            w-1/2 h-full flex flex-row justify-end items-center
            "
            >
              <button
                type="button"
                className={`
                w-16 h-6 border-2 rounded-lg text-[#fff] text-center text-[14px] font-Dosis ml-2 outline-none bg-[#000] border-[#000] hover:bg-[#fff] hover:text-[#000]
                `}
                onClick={() => {
                  selectPlateRef.current.clearValue();
                  selectVinRef.current.clearValue();
                  selectBrandRef.current.clearValue();
                  selectModelRef.current.clearValue();
                  selectVersionRef.current.clearValue();
                  selectColorRef.current.clearValue();
                  selectTransmissionRef.current.clearValue();
                  setSelectedCarId(0);
                  setAnio("");
                  setIsLocked(false);
                }}
              >
                Cancelar
              </button>

              <button
                type="button"
                className={`
                w-16 h-6 border-2 rounded-lg text-[#fff] text-center text-[14px] font-Dosis ml-2 outline-none ${
                  isLocked
                    ? "text-[#D9D9D9]"
                    : "bg-[#3682F7] border-[#3682F7] hover:bg-[#fff] hover:text-[#3682F7]"
                }
                `}
                disabled={isLocked}
                onClick={() => {
                  selectedCarId !== 0 ? editCarData() : addNewCar();
                }}
              >
                {selectedCarId !== 0 ? "Editar" : "Guardar"}
              </button>
            </div>
          </div>
        </div>
      )}
    </form>
  );
}
export default DatosVehiculos;
