import React, { useEffect, useState } from 'react';
import GaugeChart from "react-gauge-chart"

export default function GaugeChartOrdersMonth() {
    const [isLoading, setIsLoading] = useState(true)
    const [ordersCreatedThisMonth, setOrdersCreatedThisMonth] = useState([]);
    useEffect(() => {

        setIsLoading(true)

        fetch('https://slogan.com.bo/vulcano/orders/shopForemanDashboard')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    var temp = [];
                    Object.values(data.data).map((result) =>{
                        temp.push(result);
                    })
    
                    setOrdersCreatedThisMonth(temp)
                    
                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

    }, [])

    return(
       <>
            <div>
                <GaugeChart id="gauge-chart-invoicing"
                    nrOfLevels={420}
                    arcsLength={[
                        ordersCreatedThisMonth[27],
                        ordersCreatedThisMonth[28]
                    ]}
                    colors={['#71AD46', '#F6F6FA',]}
                    percent={
                        ordersCreatedThisMonth[27]
                    }
                    needleBaseColor={'#71AD46'}
                    needleColor={'#71AD46'}
                    arcPadding={0.02}
                    textColor={'#000'}
                    />
            </div>
       </>
    )
}