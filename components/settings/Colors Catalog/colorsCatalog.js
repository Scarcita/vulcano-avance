import React from 'react';
import { useEffect, useState } from 'react'
import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'
import Image from 'next/image'

const ListColorsCatalog = (props) => {
    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([]);
    const {
        setSelectedCatalog,
        reloadColorsCatalog,
        setSelectedColorsName
    } = props


    useEffect(() => {
        setIsLoading(true)
        fetch('https://slogan.com.bo/vulcano/catalogues/all/colors')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log(data.data);
                    setData(data.data)

                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })
    }, [reloadColorsCatalog])

    return (
        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 50 }}>
                    <Spinner color="#3682F7" size={14} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <div>
                <ListData 
                data={data} 
                setData={setData} 
                setSelectedCatalog={setSelectedCatalog}
                setSelectedColorsName={setSelectedColorsName}
            />
            </div>
    )
}

const ListData = (props) => {
    const { 
        data, 
        setSelectedCatalog,
        setSelectedColorsName
    } = props;

    return (
        <div className='h-[180px] bg-[#fff] rounded-[10px] shadow-md p-3 mt-[20px]'>
            {data.map(row =>
                <div key={row.id} className="flex border-b">
                    {/* <div className='m-[5px] rounded-full bg-[#F3F3F3] w-[30px] h-[30px]'>
                        {row.additional_info !== null ?
                            <Image
                                className='rounded-full bg-[#F3F3F3]'
                                src={row.additional_info}
                                alt="media"
                                height={30}
                                width={30}>

                            </Image>
                            : <></>
                        }
                    </div> */}
                    <button className='self-center text-[14px] hover:text-[#D9D9D9] focus:text-[#fff] focus:bg-[#3682F7] rounded-[4px] px-[10px] my-[5px]'
                    onClick={() => {
                        setSelectedCatalog(row.id)
                        setSelectedColorsName(row.name)
                    }}
                    >
                        {row.name}
                    </button>
                </div>
            )}
        </div>
    )
}

export default ListColorsCatalog;