import React from 'react';
import { useEffect, useState } from 'react'
import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'
import Image from 'next/image'

const ListCArsModels = (props) => {
    const { 
        selectedBrand, 
        setSelectedBrand, 
        selectedBrandVersions, 
        setSelectedBrandVersions,
        setSelectedModelsName,
        reloadCarsModels,
     } = props;
    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([]);
    const [selectedModels, setSelectedModels] = useState(0);

    console.log('Selected Brand:' + selectedBrand)

    useEffect(() => {
        if (selectedBrand != 0) {
            setIsLoading(true)
            fetch('https://slogan.com.bo/vulcano/carsModels/getByBrand/' + selectedBrand)
                .then(response => response.json())
                .then(data => {
                    if (data.status) {
                        console.log(data.data);
                        setData(data.data)

                    } else {
                        console.error(data.error)
                    }
                    setIsLoading(false)
                })
        }
    }, [selectedBrand, reloadCarsModels])

    return (
        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 50 }}>
                    <Spinner color="#3682F7" size={14} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <div>
                <ListData 
                    data={data} 
                    selectedBrand={selectedBrand} 
                    setSelectedBrand={setSelectedBrand} 
                    selectedBrandVersions={selectedBrandVersions} 
                    setSelectedBrandVersions={setSelectedBrandVersions}
                    setSelectedModelsName={setSelectedModelsName}
                    />

            </div>

    )
}

const ListData = (props) => {
    const { 
        data, 
        setSelectedBrandVersions,
        setSelectedModelsName
    } = props;

    return (
        <div className=''>
            {data.map(row =>
                <div key={row.id} className="border-b my-[12px]">
                    <button className='self-center text-[14px] hover:text-[#D9D9D9] focus:text-[#fff] focus:bg-[#3682F7] rounded-[4px] px-[10px]'
                    onClick={() => {
                        setSelectedBrandVersions(row.id)
                        setSelectedModelsName(row.name)
                    }}
                    >
                        {row.name}
                    </button>
                </div>

            )}
        </div>
    )
}

export default ListCArsModels;