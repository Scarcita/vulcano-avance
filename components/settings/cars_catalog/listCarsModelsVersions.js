import React from 'react';
import { useEffect, useState } from 'react'
import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'
import Image from 'next/image'

const ListCArsModelsVersions = (props) => {
    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([]);
    const { 
        selectedBrandVersions, 
        setSelectedBrandVersions, 
        setSelectedCarsVersions,
        reloadCarsModelsVersions,
        setSelectedVersionsName
    } = props;

    useEffect(() => {
        if(selectedBrandVersions !=0) {
            setIsLoading(true)
                fetch('https://slogan.com.bo/vulcano/carsModelsVersions/getByModel/' + selectedBrandVersions)
                .then(response => response.json())
                .then(data => {
                    if (data.status) {
                        console.log(data.data);
                        setData(data.data)

                    } else {
                        console.error(data.error)
                    }
                    setIsLoading(false)
                })
        }
    }, [selectedBrandVersions, reloadCarsModelsVersions ])

    return(
        isLoading ?
        <>
            <div className='flex justify-center items-center' style={{ width: '100%', height: 50 }}>
                <Spinner color="#3682F7" size={14} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>

        </>
        :
        <div>
            <ListData 
                data={data} 
                selectedBrandVersions={selectedBrandVersions} 
                setSelectedBrandVersions={setSelectedBrandVersions} 
                setSelectedCarsVersions={setSelectedCarsVersions}
                setSelectedVersionsName={setSelectedVersionsName}
            />
        </div>

    )
}

const ListData = (props) => {
    const { 
        data, 
        setSelectedCarsVersions,
        setSelectedVersionsName
    } = props;

    return (
        <div className=''>
            {data.map(row => 
                <div key={row.id} className="border-b my-[12px]">
                    <button 
                        className='self-center text-[14px] hover:text-[#D9D9D9] focus:text-[#fff] focus:bg-[#3682F7] rounded-[4px] px-[10px]'
                        onClick={() => {
                            setSelectedCarsVersions(row.id)
                            setSelectedVersionsName(row.name)
                        }}
                        >
                        {row.name}
                    </button>
                </div>

                )}

        </div>
    )
}

export default ListCArsModelsVersions;