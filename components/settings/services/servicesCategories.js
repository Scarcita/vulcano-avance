import React from 'react';
import { useEffect, useState } from 'react'
import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'
import Image from 'next/image'

const ServicesCategories = (props) => {
    const { 
        setSelectedServices,
        reloadServicesCategory,
        setSelectedCategoryName,
        setSelectedCategoryCode,
        setSelectedCategoryAbbreviation
    } = props;
    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([]);
    

    useEffect(() => {
        setIsLoading(true)
        fetch('http://slogan.com.bo/vulcano/servicesCategories/all')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log(data.data);
                    setData(data.data)

                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })
    }, [reloadServicesCategory])

    return (
        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 50 }}>
                    <Spinner color="#3682F7" size={14} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <div>
                <ListServices 
                    data={data} 
                    setData={setData} 
                    setSelectedServices={setSelectedServices}
                    setSelectedCategoryName={setSelectedCategoryName}
                    setSelectedCategoryCode={setSelectedCategoryCode}
                    setSelectedCategoryAbbreviation={setSelectedCategoryAbbreviation}
                    />
            </div>

    )
}

const ListServices = (props) => {
    const { 
        data,
        setSelectedServices,
        setSelectedCategoryName,
        setSelectedCategoryCode,
        setSelectedCategoryAbbreviation
     } = props;

    return (
        <div className=''>
            {data.map(row =>
                <div key={row.id} className="border-b my-[10px]">
                    <button className='self-center text-[14px] hover:text-[#D9D9D9] focus:text-[#fff] focus:bg-[#3682F7] rounded-[4px] px-[10px]'
                        onClick={() => {
                            setSelectedServices(row.id)
                            setSelectedCategoryName(row.name)
                            setSelectedCategoryCode(row.code)
                            setSelectedCategoryAbbreviation(row.abbreviation)
                        }}
                    >
                        {row.name}
                    </button>
                </div>
            )}

        </div>
    )
}

export default ServicesCategories;