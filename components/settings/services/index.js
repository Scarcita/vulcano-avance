import React, { useState, useEffect } from "react";
import ListServices from "./listServices";
import AddServices from "./modal/addServices";
import AddServicesCategories from "./modal/addServicesCategories";
import DeleteServices from "./modal/deleteServices";
import DeleteServicesCategories from "./modal/deleteServicesCategories";
import EditServices from "./modal/editServices";
import EditServicesCategories from "./modal/editServicesCategories";
import ServicesCategories from "./servicesCategories";

export default function Services(props) {
    const [selectedServices, setSelectedServices] = useState(0);
    const [selectedCategoryName, setSelectedCategoryName] = useState('');
    const [selectedCategoryCode, setSelectedCategoryCode] = useState('');
    const [selectedCategoryAbbreviation, setSelectedCategoryAbbreviation] = useState('');
    const [Services, setServices] = useState(0);
    const [selectedServicesName, setSelectedServicesName] = useState('');
    const [selectedServicesCode, setSelectedServicesCode] = useState('');
    const [selectedServicesTime, setSelectedServicesTime] = useState('');
    const [selectedServicesPrice, setSelectedServicesPrice] = useState('');
    const {
        reloadServicesCategory,
        setReloadServicesCategory, 
        reloadServices,
        setReloadServices,
    } = props


    return (
        <div className="grid grid-cols-12 gap-4 mt-[30px]">
            <div className="col-span-12 md:col-span-4 lg:col-span-6">
                <div className="justify-end flex">
                    <div>
                        <AddServicesCategories
                            reloadServicesCategory={reloadServicesCategory}
                            setReloadServicesCategory={setReloadServicesCategory}
                        />
                    </div>
                    <div>
                        <EditServicesCategories 
                            selectedServices={selectedServices}
                            selectedCategoryName={selectedCategoryName}
                            selectedCategoryCode={selectedCategoryCode}
                            selectedCategoryAbbreviation={selectedCategoryAbbreviation}
                            reloadServicesCategory={reloadServicesCategory}
                            setReloadServicesCategory={setReloadServicesCategory}
                        />
                    </div>
                    <div>
                        <DeleteServicesCategories 
                            selectedServices={selectedServices}
                            reloadServicesCategory={reloadServicesCategory}
                            setReloadServicesCategory={setReloadServicesCategory}
                        />
                    </div>
                </div>
                <div className="w-full h-[620px] bg-[#fff] rounded-[10px] shadow-md p-3 mt-[10px]">
                    <ServicesCategories 
                        selectedServices={selectedServices} 
                        setSelectedServices={setSelectedServices}
                        selectedCategoryName={selectedCategoryName}
                        setSelectedCategoryName={setSelectedCategoryName}
                        selectedCategoryCode={selectedCategoryCode}
                        setSelectedCategoryCode={setSelectedCategoryCode}
                        selectedCategoryAbbreviation={selectedCategoryAbbreviation}
                        setSelectedCategoryAbbreviation={setSelectedCategoryAbbreviation}
                        reloadServicesCategory={reloadServicesCategory}
                        setReloadServicesCategory={setReloadServicesCategory}
                    />
                </div>
            </div>
            <div className="col-span-12 md:col-span-4 lg:col-span-6">
                <div className="justify-end flex">
                    <div>
                        <AddServices 
                            selectedServices={selectedServices}
                            reloadServices={reloadServices}
                            setReloadServices={setReloadServices}
                        />
                    </div>
                    <div>
                        <EditServices 
                            selectedServices={selectedServices} 
                            Services={Services}
                            reloadServices={reloadServices}
                            setReloadServices={setReloadServices}
                            selectedServicesName={selectedServicesName}
                            selectedServicesCode={selectedServicesCode}
                            selectedServicesTime={selectedServicesTime}
                            selectedServicesPrice={selectedServicesPrice}
                        />
                    </div>
                    <div>
                        <DeleteServices 
                            Services={Services}
                            reloadServices={reloadServices}
                            setReloadServices={setReloadServices}
                        />
                    </div>
                </div>
                <div className="w-full h-[620px] bg-[#fff] rounded-[10px] shadow-md p-3 mt-[10px]">
                    <ListServices 
                        selectedServices={selectedServices} 
                        setSelectedServices={setSelectedServices} 
                        setServices={setServices}
                        reloadServices={reloadServices}
                        setReloadServices={setReloadServices}
                        selectedServicesName={selectedServicesName}
                        setSelectedServicesName={setSelectedServicesName}
                        selectedServicesCode={selectedServicesCode}
                        setSelectedServicesCode={setSelectedServicesCode}
                        selectedServicesTime={selectedServicesTime}
                        setSelectedServicesTime={setSelectedServicesTime}
                        selectedServicesPrice={selectedServicesPrice}
                        setSelectedServicesPrice={setSelectedServicesPrice}
                    />
                </div>
            </div>
        </div>
    )
}