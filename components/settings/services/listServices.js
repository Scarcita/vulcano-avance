import React from 'react';
import { useEffect, useState } from 'react'
import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'
import Image from 'next/image'

const ListServices = (props) => {
    const { 
        selectedServices, 
        setSelectedServices, 
        setServices, 
        reloadServices,
        setSelectedServicesName,
        setSelectedServicesCode,
        setSelectedServicesTime,
        setSelectedServicesPrice

    } = props;
    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([]);
    const [selectedModels, setSelectedModels] = useState(0);

    console.log('Selected Services:' + selectedServices)

    useEffect(() => {
        if (selectedServices != 0) {
            setIsLoading(true)
            fetch('http://slogan.com.bo/vulcano/services/getByCategory/' + selectedServices)
                .then(response => response.json())
                .then(data => {
                    if (data.status) {
                        console.log(data.data);
                        setData(data.data)

                    } else {
                        console.error(data.error)
                    }
                    setIsLoading(false)
                })
        }
    }, [selectedServices, reloadServices ])

    return (
            isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 50 }}>
                    <Spinner color="#3682F7" size={14} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <div>
                <ListData 
                data={data} 
                setSelectedServices={setSelectedServices} 
                setServices={setServices}
                setSelectedServicesName={setSelectedServicesName}
                setSelectedServicesCode={setSelectedServicesCode}
                setSelectedServicesTime={setSelectedServicesTime}
                setSelectedServicesPrice={setSelectedServicesPrice}
                />

            </div>

    )
}

const ListData = (props) => {
    const { 
        data, 
        setServices,
        setSelectedServicesName,
        setSelectedServicesCode,
        setSelectedServicesTime,
        setSelectedServicesPrice
    } = props;

    return (
        <div className=''>
            {data.map(row =>
                <div key={row.id} className="border-b my-[10px]">
                    <button className='self-center text-[14px] hover:text-[#D9D9D9] focus:text-[#fff] focus:bg-[#3682F7] rounded-[4px] px-[10px]'
                    onClick={() => {
                        setServices(row.id)
                        setSelectedServicesName(row.name)
                        setSelectedServicesCode(row.code)
                        setSelectedServicesTime(row.time)
                        setSelectedServicesPrice(row.price)
                    }}
                    >
                        {row.name} - {row.code} - {row.time} - {row.price}Bs.
                    </button>
                </div>

            )}
        </div>
    )
}

export default ListServices;