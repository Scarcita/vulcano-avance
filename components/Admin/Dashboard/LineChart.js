import React, { useEffect, useState } from 'react';
import Chart from "chart.js";

export default function LineChart() {
  const [isLoading, setIsLoading] = useState(true)
  const [appointmentsCreated, setAppointmentsCreated] = useState([]);

  useEffect(() => {

      setIsLoading(true)

      fetch('https://slogan.com.bo/vulcano/orders/adminDashboard')
          .then(response => response.json())
          .then(data => {
              if (data.status) {
                  var temp = [];
                  Object.values(data.data).map((result) =>{
                      temp.push(result);
                  })
  
                  setAppointmentsCreated(temp)
                  
              } else {
                  console.error(data.error)
              }
              setIsLoading(false)
          })

  }, [])

    React.useEffect(() => {
        var config = {
          type: "line",
          data: {
            labels: [
              "Jan",
              "Feb",
              "Mar",
              "Apr",
              "May",
              "Jun",
              "Jul",
              "Ago",
              "Sep",
              "Oct",
              "Nov",
              "Dec",
            ],
            datasets: [
              {
                label: new Date().getFullYear(),
                backgroundColor: "#3682F7",
                borderColor: "#3682F7",
                data: [
                  appointmentsCreated[9],
                  appointmentsCreated[10],
                  appointmentsCreated[11],
                  appointmentsCreated[12],
                  appointmentsCreated[13],
                  appointmentsCreated[14],
                  appointmentsCreated[15],
                  appointmentsCreated[16],
                  appointmentsCreated[17],
                  appointmentsCreated[18],
                  appointmentsCreated[19],
                  appointmentsCreated[20],
                ],
                fill: false,
              },

            ],
          },
          options: {
            maintainAspectRatio: false,
            responsive: true,
            title: {
              display: false,
              text: "Sales Charts",
              fontColor: "#000000",
            },
            legend: {
              labels: {
                fontColor: "#000000",
              },
              align: "end",
              position: "bottom",
            },
            tooltips: {
              mode: "index",
              intersect: false,
            },
            hover: {
              mode: "nearest",
              intersect: true,
            },
            scales: {
              xAxes: [
                {
                  ticks: {
                    fontColor: "rgba(203, 213, 225, 0.9)",
                  },
                  display: true,
                  scaleLabel: {
                    display: false,
                    labelString: "Month",
                    fontColor: "#3682F7",
                  },
                  gridLines: {
                    display: false,
                    borderDash: [2],
                    borderDashOffset: [2],
                    color: "rgba(203, 213, 225, 0.5)",
                    zeroLineColor: "rgba(0, 0, 0, 0)",
                    zeroLineBorderDash: [2],
                    zeroLineBorderDashOffset: [2],
                  },
                },
              ],
              yAxes: [
                {
                  ticks: {
                    fontColor: "rgba(203, 213, 225, 0.8)",
                  },
                  display: true,
                  scaleLabel: {
                    display: false,
                    labelString: "Value",
                    fontColor: "#3682F7",
                  },
                  gridLines: {
                    borderDash: [3],
                    borderDashOffset: [3],
                    drawBorder: false,
                    color: "rgba(203, 213, 225, 0.5)",
                    zeroLineColor: "rgba(33, 37, 41, 0)",
                    zeroLineBorderDash: [2],
                    zeroLineBorderDashOffset: [2],
                  },
                },
              ],
            },
          },
        };
        var ctx = document.getElementById("line-chartAppointmentsCreated").getContext("2d");
        window.myLine = new Chart(ctx, config);
      }, []);
      return (
        <>
          <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded bg-[#fff]">
            <div className="p-4 flex-auto">
              <div className="relative h-[230px]">
                <canvas id="line-chartAppointmentsCreated"></canvas>
              </div>
            </div>
          </div>
        </>
      );
    }