import React, { useEffect, useState } from 'react';
import Image from 'next/image'
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';

export default function Tabla() {
    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([]);
    const headlist = [
        "#OT", "Cliente", "Marca", "Modelo", "Color", "Placa", "Creacion", "Promesa Ent", "Estado"
    ];

    const getDatos = () => {
        setIsLoading(true)
        fetch('https://slogan.com.bo/vulcano/orders/all/abierto')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log(data.data);
                    setData(data.data)
                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })
    }
    useEffect(() => {
        getDatos();
    }, [])

    return (

        isLoading ?
            <div className='flex justify-center items-center self-center mt-[30px]'  >
                <Spinner color="#3682F7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
            <div >
                <div  >
                    <Table data={data} headlist={headlist}  />
                
                    <TablaResponsive headlist={headlist} data={data} />
                </div>

            </div>
    )
}

const Table = (props) => {
    const { data, headlist } = props;

    const splitData = (obj) => {
        data.forEach(element => {
            let date = element.created.split("T");
            element.created = date[0];
        });
        data = obj;
    };
    splitData(data);

    const splitData1 = (obj) => {
        data.forEach(element => {
            let date = element.car.cars_models_version.modified.split("T");
            element.car.cars_models_version.modified = date[0];
        });
        data = obj;
    };
    splitData1(data);

    return (
        <div style={{maxHeight: '245px', overflowY: 'auto'}} className=" hidden lg:block md:block">
            <table  className='w-full bg-[#fff] rounded-r-lg shadow-[25px]'>
                <thead style={{position: 'sticky', top: 0}} className='bg-[#3682F7] '>
                    <tr>
                        {headlist.map(header => <th key={headlist.id} className='text-[12px] font-semibold text-[#fff] py-2'>{header}</th>)}
                    </tr>
                </thead>
                <tbody className='divide-y '>
                 {data.map(row => 
                    <tr >
                        <td>
                            <div className="text-[10px] text-[#000000] self-center ml-[20px]">
                                {row.client_id}
                            </div>
                        </td>
                        <td>
                            <div className="text-[10px] text-[#000000] text-center self-center">
                                {row.client.name}
                            </div>
                        </td>
                        <td>
                            <div className="bg-[#fff]">
                                <Image
                                    src={row.car.cars_models_version.cars_model.catalogues_record.additional_info}
                                    alt="brand"
                                    layout="fixed"
                                    width={30}
                                    height={30}
                                />
                            </div>
                        </td>
                        <td>
                            <div className="text-[10px] text-[#000000] text-center self-center">
                                {row.car.cars_models_version.cars_model.name}
                            </div>
                        </td>
                        <td>
                            <div className="text-[10px]  text-[#000000] text-center self-center">
                                {row.car.color_id}
                            </div>
                        </td>
                        <td>
                            <div className="text-[10px] text-[#000000] text-center self-center">
                                {row.car.plate}
                            </div>
                        </td>
                        <td>
                            <div className="text-[10px] text-[#000000] text-center self-center">
                                {row.created}
                            </div>
                        </td>
                        <td>
                            <div className="text-[10px] text-[#000000] text-center self-center">
                                {row.car.cars_models_version.modified}
                            </div>
                        </td>
                        <td>
                            <div className="text-[10px] text-[#3682F7] border border-[#3682F7] text-center rounded-[15px] self-center mr-[10px]">
                                {row.status}
                            </div>
                        </td>
                    </tr>
                 )}

                </tbody>
            </table>
        </div>

    );
};
const TablaResponsive = (props) => {
    const { data, } = props;

    return (
        <div className='grid grid-cols-12 mt-[20px] '>
            <div className='col-span-12 md:col-span-12 lg:col-span-12 rounded-[10px] md:hidden'>
                
                {data.map(row =>
                    {       
                    return (
                    <div key={row.id} className='grid grid-cols-12 bg-[#fff] rounded-[15px] shadow-md px-2 py-2 mb-[15px]'>
                        <div className='col-span-12 md:col-span-12'>
                            <div className='grid grid-cols-12 gap-6'>
                                <div className='col-span-8 md:col-span-12 pt-[10px]'>
                                    <div className='grid grid-cols-12 gap-3'>
                                        <div className='col-span-12 md:col-span-12 flex flex-row justify-between'>
                                            <div className='text-[24px] font-semibold self-center'>{row.client.name}</div>
                                            <div className="w-[70px] h-[19px] bg-[#3682F7] text-[12px] text-[#FFFFFF] rounded-[20px] self-center text-center items-center ">{row.status}</div>
                                        </div>
                                        <div className='col-span-12 md:col-span-12 flex flex-row justify-between'>
                                            <div className='text-[12px] self-center'>{row.created}</div>
                                            <div className="text-[12px] text-[#000000] text-center self-center">
                                                {row.car.cars_models_version.modified}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className='col-span-4 md:col-span-12 items-center'>
                                    <div className="w-[54px] h-[54px] bg-[#F6F6FA] rounded-full self-center items-center">
                                        <Image
                                            className='rounded-full '
                                            src={row.car.cars_models_version.cars_model.catalogues_record.additional_info}
                                            alt="media"
                                            layout="fixed"
                                            width={54}
                                            height={54}
                                        />
                                    </div>
                                    <div className="text-[12px] text-[#000000] self-center">
                                        {row.car.cars_models_version.cars_model.name}
                                    </div>
                                    <div className="text-[12px]  text-[#000000] self-center">
                                        {row.car.color_id}
                                    </div>
                                    <div className="text-[12px] text-[#000000] self-center">
                                        {row.car.plate}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    )
                    }
                    )}
            </div>
        </div>

        
    );
};