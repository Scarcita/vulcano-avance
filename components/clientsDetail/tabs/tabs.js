import React, { useState, useEffect } from "react";
import Image from "next/image";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import TableOrders from "./orders/tableOrders";
import TableServices from "./services/tableServices";
import TableAppointments from "./appointments/tableAppointments";
import TableDeskSales from "./deskSales/tableDeskSales";
import TableActivites from "./activites/tableActivites";


export default function TabsClientsDetails() {


  return (
    <div className="mt-[52px]">
      <div className="grid grid-cols-12 mt-10">
        <div className="col-span-12 md:col-span-12 lg:col-span-12">
          <Tabs>
            <TabList className="grid grid-cols-10 focus:outline-none focus:text-[#3682F7] ">
              <Tab className="col-span-2 md:col-span-1 lg:col-span-1 self-center text-[12px] focus:text-[#3682F7] rounded-tl-lg px-2 py-2">
                Orders
              </Tab>
              <Tab className="col-span-2 md:col-span-1 lg:col-span-1 text-gray-500 text-[14px] self-center text-center focus:text-[#3682F7]  px-2 py-2">
                Services
              </Tab>
              <Tab className="col-span-2 md:col-span-2 lg:col-span-1 text-gray-500 text-[14px] self-center text-center focus:text-[#3682F7]  px-2 py-2">
                Appointments
              </Tab>
              <Tab className="col-span-2 md:col-span-2 lg:col-span-1 text-gray-500 text-[14px] self-center text-center focus:text-[#3682F7]  px-2 py-2">
                Desk Sales
              </Tab>
              <Tab className="col-span-2 md:col-span-1 lg:col-span-1 text-gray-500 text-[14px] self-center text-center focus:text-[#3682F7]  px-2 py-2">
                Activites
              </Tab>
            </TabList>

            <TabPanel>
              <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                  <TableOrders/>
                </div>
              </div>
            </TabPanel>

            <TabPanel>
              <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                  <TableServices/>
                </div>
              </div>
            </TabPanel>

            <TabPanel>
              <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                  <TableAppointments/>
                </div>
              </div>
            </TabPanel>

            <TabPanel>
              <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                  <TableDeskSales/>
                </div>
              </div>
            </TabPanel>

            <TabPanel>
              <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                  <TableActivites/>
                </div>
              </div>
            </TabPanel>

          </Tabs>
        </div>
      </div>
    </div>
  );
}
