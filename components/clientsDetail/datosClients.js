import React, { useEffect, useState } from "react";
import Spinner from "react-activity/dist/Spinner";
import "react-activity/dist/Spinner.css";
import Image from "next/image";

export default function DatosClients() {
    const [isLoading, setIsLoading] = useState(true);
    const [data, setData] = useState([]);

    const getDatos = () => {
        setIsLoading(true);
        fetch("https://slogan.com.bo/vulcano/clients/getByIdCrm/1")
          .then((response) => response.json())
          .then((data) => {
            if (data.status) {
                var temp = [];
                Object.values(data.data).map((result) =>{
                    temp.push(result);
                })
    
                setData(temp[0])
            } else {
              console.error(data.error);
            }
            setIsLoading(false);
          });
      };
      useEffect(() => {
        getDatos();
      }, []);

    // let spliteado = data.created.split("T");
    // let created = spliteado[0];

    return(
        isLoading ?
            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                <Spinner color="#3682F7" size={14} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
        <div>
            <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-6 flex self-center items-center">
                    <div className="bg-[#E4E7EB] w-[48px] h-[48px] rounded-full justify-center">

                    </div>
                    <div className="ml-[10px]">
                        <p className='text-[10px] font-normal'>
                            Register Into
                        </p>
                        <p className='text-[15px] font-semibold'>
                            {data.name}
                        </p>
                        <p className='text-[12px] font-normal'>
                            Created On: {data.created}
                        </p>
                    </div>
                </div>
                <div className="col-span-12 md:col-span-12 lg:col-span-3 self-center items-center">
                    <div className='text-[14px] font-semibold'>
                        Phone:
                        <span className="text-[14px] font-normal">
                            {data.phone}
                        </span>
                    </div>
                    <div className='text-[14px] font-semibold'>
                        Email:
                        <span className="text-[14px] font-normal">
                            {data.email}
                        </span>
                    </div>
                </div>
                <div className="col-span-12 md:col-span-12 lg:col-span-3 self-center items-center">
                    <p className="text-[10px] font-normal">
                        Last Order Invoicing Data
                    </p>
                    <div className='text-[14px] font-semibold'>
                        Name:
                        <span className="text-[14px] font-normal">
                            {data.invoice_name}
                        </span>
                    </div>
                    <div className='text-[14px] font-semibold'>
                        CI/NIT:
                        <span className="text-[14px] font-normal">
                            {data.invoice_cinit}
                        </span>
                    </div>
                </div>
            </div>
        </div>
    )

}